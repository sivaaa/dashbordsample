var mongoose = require('mongoose');

var activation = mongoose.model('activation', {
    activationName: String,
    date: String,
    locations: [{
        locName: String
    }]
});

module.exports = {
    activation
};