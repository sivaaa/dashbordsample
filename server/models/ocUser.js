const mongoose = require('mongoose');

const ocUserSchema = new mongoose.Schema({
   
    "currentState": { type: Number },
    "sessionTime": { type: String },
    "mediaList": [],
    "visitorData": {

        mobile: {
            type: String,
            trim: true,
            
        },
        score:{
        type:String
        },
        name: {
            type: String
        },
        email: {
            type: String,
            trim: true
        },
        picture: {
            type: String,
            trim: true
        },
        location: String,

        volunteer_mobile: {
            type: String,
            trim: true
        },
        activation: {
            type: String,
            required: true,
            trim: true
        },
        shortcode: {
            type: String,
            trim: true
        },
      
    },
    "metadataPath": { type: String },
    "error": { type: Number }

},{timestamps: true});




const ocUser = mongoose.model('ocUser', ocUserSchema);

module.exports = {ocUser}
