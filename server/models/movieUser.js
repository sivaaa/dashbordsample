const mongoose = require('mongoose');

const movieUserSchema = new mongoose.Schema({
   
    "currentState": { type: Number },
    "sessionTime": { type: String },
    "mediaList": [],
    "visitorData": {

        mobile: {
            type: String,
            trim: true,
            
        },
        score:{
        type:String
        },
        name: {
            type: String
        },
        email: {
            type: String,
            trim: true
        },
        picture: {
            type: String,
            trim: true
        },
        location:{
            type:String,
            required:true
        },

        volunteer_mobile: {
            type: String,
            trim: true
        },
        activation: {
            type: String,
            required: true,
            trim: true
        },
        shortcode: {
            type: String,
            trim: true
        },
      
    },
    "metadataPath": { type: String },
    "error": { type: Number }

},{timestamps: true});




const movieUser = mongoose.model('movieUser', movieUserSchema);

module.exports = {movieUser}
