
const express = require("express");
const morgan = require("morgan");
//const fs = require('fs');
const path = require("path");
const bodyParser = require("body-parser");
const passport = require("passport");
const app = express();

/************* mongoose setup ***************/
require("./db/mongoose");

/************* passport setup ***************/
require("./config/passport");

//***Module Variables*==============================================================================
const auth = require("./routes/authroute"),
 activation = require("./routes/activationroute");
//***Middleware*==============================================================================
// const accessLogStream = fs.createWriteStream(
//   path.join(__dirname, 'access.log'), {flags: 'a'}
// );
app.use(morgan("dev"));
// app.use(morgan('common', {stream: accessLogStream}));




app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "/client/build")));
app.use(passport.initialize());


// Add headers
app.use(function(req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,x-auth"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  res.setHeader("Access-Control-Expose-Headers", "x-auth");

  // Pass to next layer of middleware
  next();
});

app.use("/oc/user", auth);
app.use("/oc/activation",activation);
// app.use('/admin',  passport.authenticate('jwt', { session: false }), util.isAdmin, admin);

app.use((err, req, res, next) => {
    if (err.name === "UnauthorizedError") {
      res.status(401).json({ message: err.name + ": " + err.message });
    } else {
        next();
    }
  });
  
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "/client/build", "index.html"));
});

module.exports = app;