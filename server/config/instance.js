'use strict';

const instanceName = process.env.NODE_ENV || 'prod';

switch (instanceName) {
    case "dev":
        {   
            exports.instance = "dev"
            exports.port = 11001;
            exports.secret = "poijasdf98435jpgfdpoij3";
            exports.dbUrl = "mongodb://localhost:27017/originalChoiceDev";
            exports.webUrl="http://ocdev.wowso.me/";

        }
        break;
    case "prod":
        {
            exports.instance = "prod"
            exports.port = 11000;
            exports.secret = "pojiaj234oi234oij234oij4";
            exports.dbUrl = "mongodb://localhost:27017/originalChoice";
            exports.webUrl="http://oc.wowso.me/";

        }
        break;
    case "qa":
        {
            exports.instance = "qa"
            exports.port = 11002;
            exports.secret = "poijasdf98435jpgfdpoij3";
            exports.dbUrl = "mongodb://localhost:27017/originalChoiceQA";
            exports.webUrl="http://ocqa.wowso.me/";
        }   
        break;
}