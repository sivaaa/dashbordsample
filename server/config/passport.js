const passport = require("passport"),
  JwtStrategy = require("passport-jwt").Strategy,
  ExtractJwt = require("passport-jwt").ExtractJwt;

const { User } = require("../models/user");
const instance = require("../config/instance");


passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: instance.secret
    },
    (jwt_payload, done) => {
      User.findOne({ _id: jwt_payload._id })
        .then(user => {
          if (!user) {
            done(null, false, "Please Sign in!");
          } else {
            done(null, user);
          }
        })
        .catch(err => {
          console.log(err)
          done(null, false, err);
        });
    }
  )
);
