const path = require('path');
const express = require('express');
const app = express();
const publicPath = path.join(__dirname, 'build');
const fs = require('fs');
var request = require('request');

app.use(express.static(publicPath));

app.get('\*', (req, res) => {
  res.sendFile(path.join(publicPath, 'index.html'));
});

app.listen(3001, () => {
  console.log('Server is up!');
});
