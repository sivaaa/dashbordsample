import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import moment from 'moment';
import { setnewcampaigns,
         setnextcampaigns,
         activation,
         range,
         setfromtime,
         settotime,
         setfromdate,
         setmobile,
         settodate,settotalcount,setregisters,setwindowmaxvalue,initregisters} from '../../actions/index';
import PropTypes from 'prop-types';
const request = require('superagent');  

class Filters extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            cities:[]
        }  
        this.today = moment().format('YYYY-MM-DD');
        this.apiLink=process.env.REACT_APP_SECRET_CODE.trim();
        this.UpdateDateFilter = this.UpdateDateFilter.bind(this);
        this.UpdateActivationFilter = this.UpdateActivationFilter.bind(this);
        this.UpdateDateTimeFilter = this.UpdateDateTimeFilter.bind(this);     
    }

    UpdateActivationFilter(e){ 
        this.props.setmobile("")

        let {campaigns,activation,totalcount,setnewcampaigns,setnextcampaigns,registers,setregisters,settotalcount} = this.props;
        let cityName = e.target.value;             

        activation(cityName);
        console.log(cityName);
        let filterCampaigns = campaigns.filter((campaign) => {
            if(cityName !== 'all'){                
                return campaign.visitorData.activation === cityName ? campaign : ''
            }else {
                return campaign;
            }
        });    

        if(cityName !== 'all'){
            let newRegisters = {};
            newRegisters[cityName] = this.props.initregisters[cityName];
            settotalcount(newRegisters[cityName].length)
            setregisters(newRegisters)
            setnewcampaigns(filterCampaigns);
            setnextcampaigns(filterCampaigns);

            let maxValue = 0;
            let maxValues = [];
            let keyValues = Object.keys(newRegisters);
            for(let key of keyValues){
                maxValues.push(newRegisters[key].length);
            }
            maxValue = Math.max(...maxValues);
            this.props.setwindowmaxvalue(maxValue);
        }else{
            settotalcount(campaigns.length);
            setregisters(this.props.initregisters);
            setnewcampaigns(filterCampaigns);
            setnextcampaigns(filterCampaigns);

            let maxValue = 0;
            let maxValues = [];
            let keyValues = Object.keys(this.props.initregisters);
            for(let key of keyValues){
                maxValues.push(this.props.initregisters[key].length);
            }
            maxValue = Math.max(...maxValues);
            this.props.setwindowmaxvalue(maxValue);
        }
        
        
    }

    async UpdateDateFilter(e){
        let { range,
              campaigns,
              activation,
              totalcount,
              setnewcampaigns,
              setnextcampaigns,
              registers,
              actvalue,
              newcampaigns,
              nextcampaigns } = this.props;

        let duration = e.target.value; 
        await range(duration);
         
        const dateTo = moment().format('YYYY-MM-DD');
        const weeklyFrom = moment().subtract(7,'d').format('YYYY-MM-DD');
        const monthlyFrom = moment().subtract(30,'d').format('YYYY-MM-DD');

        let durationCampaigns = (ccampaigns) => {
            return ccampaigns.filter((campaign) => {                
                        if(duration !== 'all'){
                            if(duration === 'today'){
                                return moment(campaign.visitorData.date_time).format('YYYY-MM-DD') === dateTo
                            }else if(duration === 'weekly'){
                                return moment(campaign.visitorData.date_time).format('YYYY-MM-DD') >= weeklyFrom && moment(campaign.visitorData.date_time).format('YYYY-MM-DD') <= dateTo 
                            }else if(duration === 'monthly'){
                                return moment(campaign.visitorData.date_time).format('YYYY-MM-DD') >= monthlyFrom && moment(campaign.visitorData.date_time).format('YYYY-MM-DD') <= dateTo 
                            } 
                        }else {
                            return campaign;
                        }
                    }) 
        }

        let dCampaigns;
        if(this.props.actvalue !== 'all'){
            if(this.props.rangevalue !== 'all'){
                dCampaigns = durationCampaigns(newcampaigns);
                await setnextcampaigns(dCampaigns);
            }else{
                dCampaigns = durationCampaigns(newcampaigns);
                await setnewcampaigns(dCampaigns);
            }
        }else if(this.props.actvalue === 'all'){
            if(this.props.rangevalue !== 'all'){
                dCampaigns = durationCampaigns(newcampaigns);
                await setnextcampaigns(dCampaigns);
            }else{
                dCampaigns = durationCampaigns(campaigns);
                await setnewcampaigns(dCampaigns);
            }
        }

        
        let nregisters = {};

        for(let campaign of dCampaigns){  
            if(campaign.visitorData.activation !== undefined){                     
                let activation = campaign.visitorData.activation;
                if(nregisters[activation] === undefined){
                    nregisters[activation] = [];
                    nregisters[activation].push({city:campaign.visitorData.city})
                }else{
                    nregisters[activation].push({city:campaign.visitorData.city})
                }
            }
        }

        await this.props.setregisters(nregisters);
        let totalCount = 0;
        let activations = Object.keys(nregisters);
        for(let activation of activations){
            totalCount = totalCount + nregisters[activation].length
        } 

        await this.props.settotalcount(totalCount)

        let maxValue = 0;
        let maxValues = [];
        let keyValues = Object.keys(nregisters);
        for(let key of keyValues){
            maxValues.push(nregisters[key].length);
        }
        maxValue = Math.max(...maxValues);
        await this.props.setwindowmaxvalue(maxValue);        
        
        // actvalue !== 'all' ? setnewcampaigns(durationCampaigns(nextcampaigns)) : setnewcampaigns(durationCampaigns(campaigns));
    }

    async UpdateDateTimeFilter(e){
        console.log(this.today);
        const value = e.target.value;
        const name = e.target.name; 
        if(name === 'fromdate'){
            await this.props.setfromdate(value)            
        }else if(name === 'todate'){
            await this.props.settodate(value)
        }else if (name === 'fromtime'){
            await this.props.setfromtime(value)
        }else if(name === 'totime'){
            await this.props.settotime(value)
        }

        if(this.props.fromdate !== '' && this.props.todate !== '' ){
            const dateFrom = moment(this.props.fromdate).format('YYYY-MM-DD');
            const dateTo = moment(this.props.todate).format('YYYY-MM-DD');

            
            if(this.props.actvalue === 'all'){
                let {campaigns} = this.props;
                if(this.props.fromtime === '' && this.props.totime === ''){
                    let range = campaigns.filter((campaign) => {               
                        return (
                            (moment(campaign.visitorData.date_time).format('YYYY-MM-DD') >= dateFrom 
                            && moment(campaign.visitorData.date_time).format('YYYY-MM-DD') <= dateTo)  
                        )
                    })
                    let registers = {};
                    for(let campaign of range){  
                        if(campaign.visitorData.activation !== undefined){                     
                            let activation = campaign.visitorData.activation;
                            if(registers[activation] === undefined){
                                registers[activation] = [];
                                registers[activation].push({city:campaign.visitorData.city})
                            }else{
                                registers[activation].push({city:campaign.visitorData.city})
                            }
                        }
                    }

                    this.props.setregisters(registers);
                    let totalCount = 0;
                    let activations = Object.keys(registers);
                    for(let activation of activations){
                        totalCount = totalCount + registers[activation].length
                    }  

                    this.props.setnextcampaigns(range)
                    this.props.settotalcount(totalCount)

                    let maxValue = 0;
                    let maxValues = [];
                    let keyValues = Object.keys(registers);
                    for(let key of keyValues){
                        maxValues.push(registers[key].length);
                    }
                    maxValue = Math.max(...maxValues);
                    this.props.setwindowmaxvalue(maxValue);
                }else if(this.props.fromtime !== '' && this.props.totime !== ''){
                    let range = campaigns.filter((campaign) => {               
                        return (
                            (moment(campaign.visitorData.date_time).format('YYYY-MM-DD') >= dateFrom 
                            && moment(campaign.visitorData.date_time).format('YYYY-MM-DD') <= dateTo)  
                            && (moment(campaign.visitorData.date_time).format('HH:MM') >= this.props.fromtime 
                            && moment(campaign.visitorData.date_time).format('HH:MM') <= this.props.totime)
                        )
                    })
                    let registers = {};
                    for(let campaign of range){  
                        if(campaign.visitorData.activation !== undefined){                     
                            let activation = campaign.visitorData.activation;
                            if(registers[activation] === undefined){
                                registers[activation] = [];
                                registers[activation].push({city:campaign.visitorData.city})
                            }else{
                                registers[activation].push({city:campaign.visitorData.city})
                            }
                        }
                    }

                    this.props.setregisters(registers);
                    let totalCount = 0;
                    let activations = Object.keys(registers);
                    for(let activation of activations){
                        totalCount = totalCount + registers[activation].length
                    }  

                    this.props.setnextcampaigns(range)
                    this.props.settotalcount(totalCount)

                    let maxValue = 0;
                    let maxValues = [];
                    let keyValues = Object.keys(registers);
                    for(let key of keyValues){
                        maxValues.push(registers[key].length);
                    }
                    maxValue = Math.max(...maxValues);
                    this.props.setwindowmaxvalue(maxValue);
                }                
            }else{
                let {newcampaigns} = this.props;
                if(this.props.fromtime === '' && this.props.totime === ''){
                    let range = newcampaigns.filter((campaign) => {               
                        return (
                            (moment(campaign.visitorData.date_time).format('YYYY-MM-DD') >= dateFrom 
                            && moment(campaign.visitorData.date_time).format('YYYY-MM-DD') <= dateTo)  
                        )
                    })
                    let registers = {};
                    for(let campaign of range){  
                        if(campaign.visitorData.activation !== undefined){                     
                            let activation = campaign.visitorData.activation;
                            if(registers[activation] === undefined){
                                registers[activation] = [];
                                registers[activation].push({city:campaign.visitorData.city})
                            }else{
                                registers[activation].push({city:campaign.visitorData.city})
                            }
                        }
                    }

                    this.props.setregisters(registers);
                    let totalCount = 0;
                    let activations = Object.keys(registers);
                    for(let activation of activations){
                        totalCount = totalCount + registers[activation].length
                    }  

                    this.props.setnextcampaigns(range)
                    this.props.settotalcount(totalCount)

                    let maxValue = 0;
                    let maxValues = [];
                    let keyValues = Object.keys(registers);
                    for(let key of keyValues){
                        maxValues.push(registers[key].length);
                    }
                    maxValue = Math.max(...maxValues);
                    this.props.setwindowmaxvalue(maxValue);
                }else if(this.props.fromtime !== '' && this.props.totime !== ''){
                    let range = newcampaigns.filter((campaign) => {               
                        return (
                            (moment(campaign.visitorData.date_time).format('YYYY-MM-DD') >= dateFrom 
                            && moment(campaign.visitorData.date_time).format('YYYY-MM-DD') <= dateTo)  
                            && (moment(campaign.visitorData.date_time).format('HH:MM') >= this.props.fromtime 
                            && moment(campaign.visitorData.date_time).format('HH:MM') <= this.props.totime)
                        )
                    })
                    let registers = {};
                    for(let campaign of range){  
                        if(campaign.visitorData.activation !== undefined){                     
                            let activation = campaign.visitorData.activation;
                            if(registers[activation] === undefined){
                                registers[activation] = [];
                                registers[activation].push({city:campaign.visitorData.city})
                            }else{
                                registers[activation].push({city:campaign.visitorData.city})
                            }
                        }
                    }

                    this.props.setregisters(registers);
                    let totalCount = 0;
                    let activations = Object.keys(registers);
                    for(let activation of activations){
                        totalCount = totalCount + registers[activation].length
                    }  

                    this.props.setnextcampaigns(range)
                    this.props.settotalcount(totalCount)

                    let maxValue = 0;
                    let maxValues = [];
                    let keyValues = Object.keys(registers);
                    for(let key of keyValues){
                        maxValues.push(registers[key].length);
                    }
                    maxValue = Math.max(...maxValues);
                    this.props.setwindowmaxvalue(maxValue);
                }
            }
            
        }
    }
    
    componentDidMount(){
        this.props.activation("all");
        this.props.range("all");
        this.props.setfromtime("");
        this.props.settotime("");
        this.props.setfromdate("");
        this.props.settodate("");
        request
            .get(this.apiLink+'oc/activation/getuniquecities')
            .then(res => {
                let city = JSON.parse(res.text);
                console.log('cities:',city.todos)
                this.setState({
                    cities: [
                        ...city.todos
                    ]
                })
            });
    }    
    componentWillMount(){

    }
    
    render(){
        let {registers = {}} = this.props;
        let activations = Object.keys(registers);       
        
        
        return (
            <div className="filters">
                <div className="filters-in">
                    <div className="activations select">
                        <select onChange={this.UpdateActivationFilter} value={this.props.actvalue} className={this.props.actvalue === 'all' ? "selectOpt all" :"selectOpt"} required>
                            <option value="all">All Activations</option>
                            {this.state.cities.map((van) => (<option value={van} key={van}>{van}</option>))}                           
                        </select>
                    </div>
                 
                    <div className="date">
                        <input 
                            type="date" 
                            placeholder="From" 
                            name="fromdate" 
                            onChange={this.UpdateDateTimeFilter} 
                            value={this.props.fromdate}
                            max={this.props.todate?this.props.todate:this.today}
                            id="fromdate" required />
                        <input 
                            type="date" 
                            placeholder="To" 
                            name="todate" 
                            min={this.props.fromdate}
                            onChange={this.UpdateDateTimeFilter} 
                            value={this.props.todate}
                            // max={this.today}
                            id="todate" required />
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        actvalue: state.activeFilter.actvalue,
        rangevalue: state.activeFilter.rangevalue,
        fromdate:state.activeFilter.fromDate,
        todate:state.activeFilter.toDate,
        fromtime:state.activeFilter.fromTime,
        totime:state.activeFilter.toTime,
        campaigns:state.activeFilter.campaigns,
        newcampaigns:state.activeFilter.newcampaigns,
        registers:state.activeFilter.registers,
        initregisters:state.activeFilter.initregisters,
        totalcount:state.activeFilter.totalcount,
        nextcampaigns:state.activeFilter.nextcampaigns
    };
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        setnewcampaigns:setnewcampaigns,
        setnextcampaigns:setnextcampaigns,
        setregisters:setregisters,
        setwindowmaxvalue:setwindowmaxvalue,
        activation:activation,
        range:range,
        setfromdate:setfromdate,
        setmobile:setmobile,
        settodate:settodate,
        setfromtime:setfromtime,settotime:settotime,settotalcount:settotalcount}, dispatch);
}

export default connect( mapStateToProps,matchDispatchToProps)(Filters);
