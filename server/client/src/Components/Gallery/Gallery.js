import React, { Component } from 'react';
import Filters from './Filters';
import moment from 'moment';
import { NavLink,withRouter } from 'react-router-dom';
import AuthService from './../../routers/AuthService';
import withAuth from './../../routers/withAuth';
import Header from '../Header/Header';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import InfiniteLoader from 'react-infinite-loader'
import {setcampaigns,
    setnewcampaigns,
    setnextcampaigns,
    setinitregisters,
    setregisters,
    settotalcount,
    setmobilecampaigns,
    setmobile} from '../../actions/index';

const request = require('superagent');


class Gallery extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            gallery:true,
            items :[],
            count:8,
        }
        this.handleClick = this.handleClick.bind(this);        
        this.mobileNumberSearch = this.mobileNumberSearch.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
        const Auth = new AuthService('http://api.wowso.me/tvs');
    }
    componentDidMount(){
        this.loadItems()
    }
    componentWillMount(){
        if(this.props.login === false){
            localStorage.removeItem('id_token');
            this.props.history.push({
                pathname: '/advlogin'
            })
        }
    }

    handleLogout(){
        this.Auth.logout()
        this.props.history.replace('/advlogin');
    }

    mobileNumberSearch(e){
        let {campaigns,mobile} = this.props;
        console.log(e.target.value);
        this.props.setmobile(e.target.value)
        if(e.target.value){
        let filterCampaigns = campaigns.filter((campaign) => {
            return e.target.value.length > 0 ? (campaign.visitorData.mobile.startsWith(e.target.value) ? campaign : '') : campaign
        })
        this.props.setmobilecampaigns(filterCampaigns)

    }
    else{
        this.props.setmobilecampaigns(campaigns)

    }
    }
   
    handleClick(e,campaign){
        e.preventDefault();
        let id = campaign.visitorData.shortcode.split('me/')[1];
        this.props.history.push({
            pathname: `/gallery/${campaign.visitorData.shortcode}`,
            search: '',
            state: { detail: campaign }
        })
    }
    loadItems() {
        /* just simulating a load of more items from an api here */
        setTimeout( () => {
        //   let items = this.state.items.slice()
        //   items = items.concat(this.getItems())
          this.setState({ count: this.state.count+4 })
        }, 1000)
      }
    
      handleVisit () {
        this.loadItems()
      }
    
      getItems() {
        let items = []
        for(var i = 0; i < 10; i++) {
          items.push({ name: 'An item' })
        }
        return items
      }
    
      renderCards() {
        const { items } = this.state
        const cards = items.map((item, i) => {
          return (
            <div key={i}><h3>{item.name}</h3></div>
          )
        })
        return cards
      }
    
    render() {
        let UpdateFilter = (props) => {
            let _campaigns;
           
            if(this.props.actvalue === 'all'){
                if(this.props.mobile!==''){
                    _campaigns=props.mobileCampaigns;

                }
               else if(this.props.fromdate === '' && this.props.todate === ''){
                    _campaigns = props.campaigns;
                }else if(this.props.fromdate !== '' && this.props.todate !== ''){
                    _campaigns=this.props.campaigns.filter((campaign)=>{
                        console.log(campaign);
                        return (
                            (moment(campaign.sessionTime).format('YYYY-MM-DD') >= this.props.fromdate
                                && moment(campaign.sessionTime).format('YYYY-MM-DD') <= this.props.todate)
                        )
                    })
                    console.log(_campaigns,this.props.campaigns);
                }else{
                    _campaigns = this.props.campaigns;
                }               
            }else if(this.props.actvalue !== 'all'){
                if(this.props.fromdate !== '' && this.props.todate !== ''){
                    _campaigns=this.props.campaigns.filter((campaign)=>{
                        console.log((moment(campaign.sessionTime).format('YYYY-MM-DD')));
                        return (
                            (moment(campaign.sessionTime).format('YYYY-MM-DD') >= this.props.fromdate
                                && moment(campaign.sessionTime).format('YYYY-MM-DD') <= this.props.todate && campaign.visitorData.activation===this.props.actvalue)
                        )
                    })
                    console.log(_campaigns,this.props.campaigns);

                }
                else{
                    _campaigns = props.nextCampaigns;
                }
            }
        
            return _campaigns.slice(0,this.state.count).map((campaign, index) => {
                let visitorData = campaign.visitorData;                            
                let Date = moment(campaign.sessionTime).format("YYYY-MM-DD");
                let Time = moment(campaign.sessionTime).format("HH:mm");
                return (
                    <div className="extmtr">
                    <div className="img-block" 
                            key={index} 
                            onClick={(e) => this.handleClick(e,campaign)}>
                        <div className="img-block-in" >
                            <div className="campaign-block">
                                <div className="activation-details">
                                    <span className="act-date">{Date}</span>
                                    <span className="act-time">{' ' + Time} </span>
                                    <span className="act-area">{visitorData.city}</span>
                                    <span className="act-num">{visitorData.activation}</span>
                                </div>
                            </div>
                            {visitorData && visitorData.picture && <div className="campaign-img">
                                <img src={visitorData.picture} />
                            </div>}
                            
                        </div>
                    </div>
                   { index+1==this.state.count &&  <div className="loadimg">
                     {/* { this.renderCards() } */}
                     <InfiniteLoader onVisited={ () => this.handleVisit() } />
                   </div>}
                   </div>
                )
                   
            })
        }
        return (
            <div>
                <Header pageType="exitometer" gallery={this.state.gallery}/>
                <div className="main">
                    <div className="main-top">
                        <div className="search-gal">
                            <input 
                                name="search" 
                                placeholder="Search by Phone Number"  autoComplete="off"
                                type="number"
                                onChange={this.mobileNumberSearch}
                                value={this.props.mobile} />
                            <button className="search-click"><img src="/assets/images/searchicon-l.png" alt="" /></button>
                        </div>
                        <Filters />
                    </div>
                    <div className="gallery-main">
                        {this.props.campaigns && 
                             <UpdateFilter key="33"
                                campaigns={this.props.campaigns}
                                nextCampaigns={this.props.nextCampaigns}
                                newCampaigns={this.props.newCampaigns}
                                mobileCampaigns={this.props.mobileCampaigns} />}

                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        actvalue: state.activeFilter.actvalue,
        rangevalue: state.activeFilter.rangevalue,
        campaigns: state.activeFilter.campaigns,
        newCampaigns: state.activeFilter.newcampaigns,
        nextCampaigns: state.activeFilter.nextcampaigns,
        fromdate:state.activeFilter.fromDate,
        todate:state.activeFilter.toDate,
        registers:state.activeFilter.registers,
        totalcount:state.activeFilter.totalcount,
        mobile:state.activeFilter.mobile,
        mobileCampaigns:state.activeFilter.mobileCampaigns,
        login:state.activeFilter.login,
        mobile:state.activeFilter.mobile
    };
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({setcampaigns: setcampaigns,
        setnewcampaigns:setnewcampaigns,
        setnextcampaigns:setnextcampaigns,
        setinitregisters:setinitregisters,
        setmobile:setmobile,
        setmobilecampaigns:setmobilecampaigns,
        setregisters:setregisters,settotalcount:settotalcount}, dispatch);
}

export default withAuth(withRouter(connect( mapStateToProps,matchDispatchToProps)(Gallery)));