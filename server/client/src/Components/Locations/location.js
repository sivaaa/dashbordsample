import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink,withRouter } from 'react-router-dom';
import AuthService from './../../routers/AuthService';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import withAuth from './../../routers/withAuth';
import {setanalytics,setgallery} from '../../actions/index';
import moment from 'moment';

const request = require("superagent");

class Location extends Component {
    constructor(props) {
        super(props);
        this.state = {     
            activations:[] ,
            activation:"", 
            date:"",
            disablelength:0,
            locations:[
        ],
        }
        this.activationName='';
        this.apiLink = process.env.REACT_APP_SECRET_CODE.trim();
        this.updateActivation=this.updateActivation.bind(this);
        this.today = moment().format('YYYY-MM-DD');
        this.setDate =this.setDate.bind(this);
        this.updateLocations=this.updateLocations.bind(this);
        this.addLocation=this.addLocation.bind(this);
        this.submitLocations=this.submitLocations.bind(this);
    }
   
      
    updateActivation(e){
        console.log(e.target.value,"hhhhhh");
        let activation=e.target.value
        this.setState({
            activationName:e.target.value
        })
       if(this.state.date!==''){
        request
        .post(this.apiLink + "oc/activation/getLocations")
        .send({activationName:activation,date:this.state.date})
        .then(res => {
            console.log(res.body.message);
            this.setState({
                locations:res.body.message,
                disablelength:res.body.message.length
            })
            
           
        })
        .catch(e => {
            console.log(e);
        }); 
       }
       
    }
    submitLocations(){
        let locations=this.state.locations.filter((location)=>{
            if(location.locName!==''){
                return location
            }
        })
        if(this.state.activationName!=='' && this.state.date!=='' && locations.length>this.state.disablelength){
            console.log('dddddddd');
            request
            .post(this.apiLink + "oc/activation/addLocations")
            .send({activationName:this.state.activationName,date:this.state.date,locations:this.state.locations})
            .then(res => {
                console.log(res.body.message);

              
              
               
            })
            .catch(e => {
                console.log(e);
            }); 
        }
    }
    addLocation(){
        let obj={
            locName:""
        }
        this.setState({ locations: [...this.state.locations, obj] })
    }
    updateLocations(e,index){
        let locations=this.state.locations;
        locations[index].locName=e.target.value;
        this.setState({
            locations:locations
        })
        console.log(e.target.value,index);
    }
    setDate(e){
        let date=moment(e.target.value).format("DD-MM-YYYY");
    console.log(date);
    this.setState({
        date:date
    })
    if(this.state.activationName!==''){
        request
        .post(this.apiLink + "oc/activation/getLocations")
        .send({activationName:this.state.activationName,date:date})
        .then(res => {
            console.log(res.body.message);
            this.setState({
                locations:res.body.message,
                disablelength:res.body.message.length

            })
           
        })
        .catch(e => {
            console.log(e);
        }); 
    }
   
    }
    
    componentWillMount(){
       
    }
    componentDidMount(){
        let token=localStorage.getItem("id_token")
        if (!token) {
            localStorage.removeItem("id_token");
            this.props.history.push({
              pathname: "/advlogin"
            });
          }
          else{
            request
            .get(this.apiLink + "oc/activation/locations")
            .then(res => {
                console.log(res.body.message.vanNames);
                this.setState({
                    activations:res.body.message.vanNames,
                    activationName:res.body.message.vanNames[0]?res.body.message.vanNames[0]:""
                })
            })
            .catch(e => {
                console.log(e);
            }); 
          }
    }
    render(){
            return (
                <div>
                <div className="dropdown">
                <input value={this.state.activationName} onChange={this.updateActivation} type="text" />
                <select value={this.state.activationName} onChange={this.updateActivation}>
                    {this.state.activations && this.state.activations.map((activation,index)=>(
                    <option key={index} value={activation}>{activation}</option>

                    ))}
                </select>
            </div>
            <div className="date">
                                    <input
                                        type="date"
                                        placeholder="Set Date"
                                        name="fromdate"
                                        min={this.today}
                                        onChange={this.setDate}
                                        id="fromdate" required />
                                   
                                </div>
                                <div>
                                    {this.state.locations && this.state.locations.map((loc,index)=>(
                                        <input key={index} type="text" value={loc.locName} disabled={index<this.state.disablelength?true:false} onChange={(e)=>this.updateLocations(e,index)} ></input>
                                    ))}
                                </div>
                                <span onClick={this.addLocation}>Add location</span>
                                <button onClick={this.submitLocations}>Submit</button>
            </div>
            );
        
       
    }
}

function mapStateToProps(state) {
    return {        
    };
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        }, dispatch);
}

export default withRouter(connect( mapStateToProps,matchDispatchToProps)(Location));