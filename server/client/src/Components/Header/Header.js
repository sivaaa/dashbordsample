import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink,withRouter } from 'react-router-dom';
import AuthService from './../../routers/AuthService';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {setlogout,setlandingpage} from '../../actions/index';
import withAuth from './../../routers/withAuth';
import {setanalytics,setgallery} from '../../actions/index';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {   
            showHeaderLinks:true,    
        }
        this.changePage=this.changePage.bind(this);
        this.redirect=this.redirect.bind(this);
        this.route=this.route.bind(this);
        this.route2=this.route2.bind(this);
    }
    changePage(e,page){
        if(this.props)
        if(page==='gallery'){
            this.props.history.push({
                pathname: '/movie/gallery'
            })
        }
        else{
            this.props.history.push({
                pathname: '/yourmovie'
            })
        }
        
        this.props.setlandingpage(page);
        this.props.onSelectLanguage(page);            
    
    }
    route(e){
        console.log(this.props.landingpage);
    if(this.props.landingpage==='gallery'){
        this.props.history.push({
            pathname: '/gallery'
        })
    }
    else{
        this.props.history.push({
            pathname: '/advance'
        })
    }
    }
    route2(e){
        if(this.props.landingpage==='gallery'){
            this.props.history.push({
                pathname: '/movie/gallery'
            })
        }
        else{
            this.props.history.push({
                pathname: '/yourmovie'
            })
        }
        
    }
    logout(e){
        e.preventDefault();        
        this.props.setlogout(false);
        const Auth = new AuthService('http://api.wowso.me/tvs');
        Auth.logout();
        this.props.history.push({
            pathname: '/advlogin'
        })
    }   
    redirect(e,page){
        console.log(page)
        if(page==='advance'){
            this.props.setlandingpage('analytics')
        }
        else{
            this.props.setlandingpage('gallery')
        }
        this.props.history.push({
            pathname: '/'+page
        })
    }

    
    componentWillMount(){
        console.log(this.props.pageType);
        if(this.props.logout === undefined){
            this.props.setlogout(true)
        }else if(this.props.logout !== undefined && this.props.logout === false){
            this.props.setlogout(true)
        }
    }
    componentDidMount(){
        // this.props.setlandingpage('analytics')
        console.log(this.props.match.params.id);
    if(this.props.match.params.id){
        console.log(window.location.href.split('/')[3])
       if( window.location.href.split('/')[3]==='yourmovie'){
        this.setState({
            showHeaderLinks:false,
         })
       }
    
    }
    }
    render(){
        if(this.props.pageType==='exitometer'){
            return (
                <div className="header">
                    <div className="header-main">
                        <div className="logo">
                        <a><img src="/assets/images/logo.jpg"/></a>                    </div>   
                        <div className="header-links">
                            <a onClick={(e)=>this.redirect(e,'advance')}  className={this.props.landingpage === "gallery" ? '' : 'active'} >Analytics</a>
                            {/* <NavLink to="/analytics" className={this.props.analytics !== undefined && this.props.analytics ? 'active' : ''} >Analytics</NavLink> */}
                            <a onClick={(e)=>this.redirect(e,'gallery')}  className={this.props.landingpage === "gallery" ? 'active' : ''} >Gallery</a>
                            {this.props.logout && <a onClick={this.logout.bind(this)}>Logout</a>}
                        </div> 
                    </div>    
                    <div className="top-btns">
                        <div>
                            <button className="active">Excitometer</button>
                            <button onClick={(e) => this.route2(e)}>Your Movie</button>
                        </div>
                    </div>    
                </div>
            );
        }
        else{
            return (
                <div className={this.state.showHeaderLinks===true?"header":"header ym_contact"}>
                    <div className="header-main">
                        <div className="logo">
                        <a><img src="/assets/images/logo.jpg"/></a></div>   
                        {this.state.showHeaderLinks && this.state.showHeaderLinks===true && (
                            <div className="header-links">
                                <a onClick={(e) => this.changePage(e, 'analytics')} className={this.props.landingpage === "gallery" ? '' : 'active'} >Analytics</a>
                                {/* <NavLink to="/analytics" className={this.props.analytics !== undefined && this.props.analytics ? 'active' : ''} >Analytics</NavLink> */}
                                <a onClick={(e) => this.changePage(e, 'gallery')} className={this.props.landingpage === "gallery" ? 'active' : ''} >Gallery</a>
                                {this.props.logout && <a onClick={this.logout.bind(this)}>Logout</a>}
                            </div>  
                        )}
                          
                    </div>  
                    {this.state.showHeaderLinks && this.state.showHeaderLinks===true && (
                        <div className="top-btns">
                        <div>
                            <button onClick={(e) => this.route(e)}>Excitometer</button>
                            <button className="active">Your Movie</button>
                        </div>
                    </div> 
                    )}  
                            
                </div>
            );
        }
       
    }
}

function mapStateToProps(state) {
    return {        
        logout:state.activeFilter.logout,
        landingpage:state.activeFilter.landingpage

    };
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        setlandingpage:setlandingpage,
        setlogout:setlogout}, dispatch);
}

export default withRouter(connect( mapStateToProps,matchDispatchToProps)(Header));