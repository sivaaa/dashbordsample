import React, { Component } from 'react';
import {
    withRouter
  } from 'react-router-dom'
  import { bindActionCreators } from 'redux';
  import { connect } from 'react-redux';
  import { setlogin,setlogout,setcampaigns,
    setnewcampaigns,
    setnextcampaigns,
    setinitregisters,
    setregisters,
    setLocation,
    setcampainstree,
    setinitregistersLoc,
    setregistersLoc,
    settotalcount,
    setwindowmaxvalue,
    setanalytics,
    setlandingpage,
    setgallery } from '../../actions/index';
import AuthService from './../../routers/AuthService';
// import Location from './../Analytics/MapLocation';
import './Login.css';
const request = require('superagent');

class AdvLogin extends Component {
  constructor(props){
    super(props);
    console.log(this.props);

    this.state = {   
        username:'',
        password:'',
        login:true ,
        spinner:false 
    }; 
    this.apiLink=process.env.REACT_APP_SECRET_CODE.trim();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.Auth = new AuthService(this.apiLink+'oc');
  }
  
  handleSubmit(e){
    e.preventDefault();
    let {username,password} = this.state;
    this.setState({
        spinner:true
    })
    this.Auth.login(username,password)
        .then(res =>{
            if(this.props){

            this.props.setlogin(true)
            this.props.setLocation('all');
            this.props.setlandingpage("analytics");
            this.setState({
                spinner:false
            })
            console.log(this.props);
           this.props.history.push({pathname:'/advance'});
        }
        })
        .catch(err =>{
           this.setState({
                login: false,
                spinner:false
            })
        })
  }

  componentWillMount(){
    if(this.Auth.loggedIn() && this.props.login === true){        
        this.props.history.replace('/analytics');
    }
  }
  handleChange(e){
      let name = e.target.name;
      let value = e.target.value;
      //console.log(name,value)
      this.setState({
          [name]:value
      })
  }
  render() {  
    return (
        <div className="signin-block">
            <form onSubmit={this.handleSubmit} autoComplete="off">
                <h3>Sign in</h3>
                <label className="name">
                    <input 
                        type="text" 
                        name="username" 
                        onChange={this.handleChange}
                        value={this.state.username} 
                        placeholder="User Name" autoComplete="off"  />
                </label>
                <label className="password">
                    <input 
                        type="password" 
                        name="password" 
                        placeholder="Password" 
                        onChange={this.handleChange}
                        value={this.state.password} autoComplete="off" 
                        />
                </label>
                <p><input 
                    type="submit" 
                    className="btn-login" 
                    name="login" 
                    value="Login"
                    />
                </p>
                {this.state.login === false && <p className="invalid error">Please enter valid details</p>}
            </form>
            <p className="btm-line">Having an issue to login? <a >Contact Us</a></p>
            {this.state.spinner && <div className="spinner">
                <img src="assets/images/spinner.gif" width="45px" height="45px" />
            </div>}
        </div>
    );
  }
}

//export default withRouter(Login);
//this.props.setlogin(true);
function mapStateToProps(state) {
    return {        
        // login:state.activeFilter.login
        actvalue: state.activeFilter.actvalue,
        rangevalue: state.activeFilter.rangevalue,
        campaigns: state.activeFilter.campaigns,
        newCampaigns: state.activeFilter.newcampaigns,
        nextCampaigns: state.activeFilter.nextcampaigns,
        fromdate:state.activeFilter.fromDate,
        todate:state.activeFilter.toDate,
        campaintree:'state.activeFilter.campaintree',
        registers:state.activeFilter.registers,
        totalcount:state.activeFilter.totalcount,
        analytics:state.activeFilter.analytics,
        gallery:state.activeFilter.gallery,
        login:state.activeFilter.login
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        setlogin:setlogin,
        setLocation:setLocation,
        setcampaigns: setcampaigns,
        setnewcampaigns:setnewcampaigns,
        setnextcampaigns:setnextcampaigns,
        setinitregisters:setinitregisters,
        setregisters:setregisters,
        setcampainstree:setcampainstree,
        setinitregistersLoc:setinitregistersLoc,
        setregistersLoc:setregistersLoc,
        settotalcount:settotalcount,
        setwindowmaxvalue:setwindowmaxvalue,
        setanalytics:setanalytics,
        setgallery:setgallery,
        setlogout:setlogout,
        setlandingpage:setlandingpage,

    }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(AdvLogin);