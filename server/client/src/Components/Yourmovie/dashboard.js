import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import AuthService from './../../routers/AuthService';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withAuth from './../../routers/withAuth';
import { setanalytics, setgallery,setlandingpage } from '../../actions/index';
import Header from "../Header/Header";
import moment from 'moment';
import * as $ from "jquery";
import InfiniteLoader from 'react-infinite-loader';
import * as html2canvas from 'html2canvas';


const request = require("superagent");

class Yourmovie extends Component {
    constructor(props) {
        super(props);
        this.state = {
            registeredData:[],
            maxValue:0,
            graphData: [],
            filteredgraphData:[],
            activation: "all",
            locationData: [],
            location: "all",
            filteredData: [],
            filterData2:[],
            pageType: this.props.landingpage?this.props.landingpage:"analytics",
            fromDate: "",
            todate: "",
            pageView:"",
            mobile:'',
            totalCampaigns:10,
            count:8,
        }
        this.mindate = "";
        this.maxdate = "";
        this.totalCampaigns=10;
        this.today = moment().format('YYYY-MM-DD');
        this.apiLink = process.env.REACT_APP_SECRET_CODE.trim();
        this.UpdateActivationFilter = this.UpdateActivationFilter.bind(this);
        this.UpdateLocationFilter = this.UpdateLocationFilter.bind(this);
        this.picture = this.picture.bind(this);
        this.handleLanguage = this.handleLanguage.bind(this);
        this.UpdateDateOC = this.UpdateDateOC.bind(this);
        this.detail=this.detail.bind(this);
        this.mobileNumberSearch=this.mobileNumberSearch.bind(this);
        this.handleData=this.handleData.bind(this);
        this.handleVisit=this.handleVisit.bind(this);
    }
    handleVisit(){
        setTimeout( () => {
              this.setState({ count: this.state.count+4 })
            }, 1000)
    }
    handleData(e,count){
        console.log(count);
        setTimeout(() => {
            this.setState({
              totalCampaigns: count+10
            });
          }, 1000);
    }
    handleLanguage(page) {
        console.log(page);
        this.mindate="";
        this.maxdate='';

        this.setState({
            pageType: page,
            pageView:"analytics",
            mobile:"",
            activation:"all",
            location:"all",
            totalCampaigns:10,

        })
        this.getFilteredGraph();
        this.getFilteredDate(null,null);
    }
    mobileNumberSearch(e){
        const reg = /^[0]?[789]\d{9}$/;
        console.log(reg.test(e.target.value));
        this.setState({
            activation:"all",
            location:"all"
        })
        this.mindate="";
        this.maxdate='';
        this.setState({
            mobile:e.target.value
        })
       console.log(e.target.value)
        if(e.target.value){
        let filterCampaigns = this.state.registeredData.filter((campaign) => {
            return e.target.value.length > 0 ? (campaign.visitorData.mobile.startsWith(e.target.value) ? campaign : '') : campaign
        })
        console.log(filterCampaigns);
        this.setState({
            filteredData:filterCampaigns,
        })
    }
    else{
            this.setState({
                filteredData: this.state.registeredData
            })
    }
    }
    detail(e, campaign) {
        console.log(campaign)
        let sCode=campaign.visitorData.shortcode;
        this.props.history.push({
            pathname: '/record/'+sCode
        })
        this.props.setlandingpage("gallery");

    //    this.setState({
    //        campaign:campaign,
    //        pageView:"gallery"
    //    })
      }
    UpdateDateOC(e) {
        console.log(e.target.value, e.target.name)
        let date = e.target.value;
        if (e.target.name === 'fromdate') {
            this.mindate = date;

        }
        else {

            this.maxdate = date;

        }
        if (this.mindate !== '' || this.maxdate !== '') {
            console.log(this.mindate, this.maxdate);
            this.getFilteredDate(this.state.activation === "all" ? null : this.state.activation, this.state.location === "all" ? null : this.state.location)
        }

    }
    exportCSVFile(headers, items, fileTitle) {
        if (headers) {
          items.unshift(headers);
        }
    
        // Convert Object to JSON
        var jsonObject = JSON.stringify(items);
    
        var csv = this.convertToCSV(jsonObject);
    
        var exportedFilenmae = fileTitle + '.csv' || 'export.csv';
    
        var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
          navigator.msSaveBlob(blob, exportedFilenmae);
        } else {
          var link = document.createElement("a");
          if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", exportedFilenmae);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
          }
        }
      }
      convertToCSV(objArray) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';
    
        for (var i = 0; i < array.length; i++) {
          var line = '';
          for (var index in array[i]) {
            if (line != '') line += ','
    
            line += array[i][index];
          }
    
          str += line + '\r\n';
        }
    
        return str;
      }
    handleExport(e,data) {
    console.log(e,data)
    // let data=this.state.filteredData;
    const itemsFormatted = [];
    
    for(let i=0;i<data.length;i++){
    let obj={};
    let userData=data[i].visitorData;
    obj.name=userData.name?userData.name:" ";
    obj.mobile=userData.mobile?userData.mobile:" ";
    obj.email=userData.email?userData.email:" ";
    obj.activation=userData.activation;
    obj.location=userData.location;
    obj.date=moment(data[i].sessionTime).format("YYYY-MM-DD");
    obj.time=moment(data[i].sessionTime).format("HH:mm");
    obj.picture=userData.picture
    itemsFormatted.push(obj);
    }
    console.log(itemsFormatted);
    var headers = {
      name: 'Name'.replace(/,/g, ''), // remove commas to avoid errors
      mobile: "Mobile",
      email: "Email",
      activation: "Activation",
      location: "Location",
      date:"Date",
      time:"time",
      picture: "Picture"
    };
    var fileTitle = 'UserList'; // or 'my-unique-title'

    this.exportCSVFile(headers, itemsFormatted, fileTitle);
    
  }
    UpdateActivationFilter(e) {
        this.mindate = '';
        this.maxdate = "";
        console.log(e.target.value)
        this.setState({
            activation: e.target.value,
            location:'all',
            mobile:""

        });
        if (e.target.value !== 'all') {
            let locations = this.state.graphData.filter((act) => {
                if (act.activation === e.target.value) {
                    return act.location
                }
            })
            console.log(locations);
            let locationsData=[];
            locationsData=locations[0].location;
            setTimeout( () => {
                locationsData= locationsData.sort(function(a, b) {
                    if (a && b){
                   var textA = a.toUpperCase();
                   var textB = b.toUpperCase();
                   return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                }
               });
               console.log(locationsData)
                this.setState({

                    locationData: locationsData,

                });
             }, 900);
            // this.setState({
            //     locationData: locations[0].location
            // })
            // console.log(locations[0].location);
            this.getFilteredDate(e.target.value);
        }
        else {
            this.setState({
                location:"all"
            })
            this.getFilteredGraph();
            this.getFilteredDate();

        }

    }
    picture(e, url) {
        var milliseconds = (new Date).getTime();
        var xhr = new XMLHttpRequest();
        var img = url.replace(/^https:\/\//i, "http://");
        xhr.open("GET", img, true);
        xhr.responseType = "blob";
        xhr.onload = function () {
            var urlCreator = window.URL || window.webkitURL;
            var imageUrl = urlCreator.createObjectURL(this.response);
            var tag = document.createElement("a");
            tag.href = imageUrl;
            tag.download = `${milliseconds}.png`;
            document.body.appendChild(tag);
            tag.click();
            document.body.removeChild(tag);
        };
        xhr.send();
    }
    UpdateLocationFilter(e) {
        this.mindate = '';
        this.maxdate = "";
        console.log(e.target.value);
        this.setState({
            location: e.target.value
        })
        if (e.target.value !== 'all') {
            this.getFilteredDate(this.state.activation, e.target.value);
        }
        else {
            this.getFilteredDate(this.state.activation);

        }
    }
    updateGraphData(){
        if(this.state.activation!=='all'){
            
            if(this.state.location!=='all'){
                let object=[];
                let obj={};
                obj.activation=this.state.location;
                obj.total=this.state.filteredData.length;
                object.push(obj);
                this.setState({
                    filteredgraphData:object,
                    maxValue:obj.total
                })
            }
            else{
                let objfind=this.state.graphData.filter((data)=>{
                    if(data.activation===this.state.activation){
                        return data;
                    }
                 })
                 console.log(objfind[0]);
                 
                 let listData=[];
                 let locArray=[];
                 if(objfind[0])
                 locArray=objfind[0].location;
                 for(let i=0;i<locArray.length;i++){
                     let objCount=this.state.filteredData.filter((data)=>{
                         return data.visitorData.location===locArray[i]
                     })
                     let obj={};
                     obj.activation=locArray[i];
                     obj.total=objCount.length;
                     listData.push(obj);

                 }
                 console.log(listData);
                 setTimeout( () => {
                    listData= listData.sort(function(a, b) {
                        if (a.activation && b.activation){
                       var textA = a.activation.toUpperCase();
                       var textB = b.activation.toUpperCase();
                       return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                    }
                   });
                     this.setState({
                        
                         filteredgraphData: listData,
                         maxValue: Math.max.apply(Math, listData.map(function (o) { return o.total; }))

                     })
                 }, 900);
                 
            }
        }
        else{
            console.log(this.state.graphData);
            this.setState({
                maxValue:Math.max.apply(Math,this.state.graphData.map(function(o) { return o.total; }))
            })
        }
        if(this.mindate!=='' && this.maxdate!==''){
            if(this.state.activation!=='all'){
            if(this.state.location==='all'){
                let objfind=this.state.graphData.filter((data)=>{
                    if(data.activation===this.state.activation){
                        return data;
                    }
                 })
                 console.log(objfind[0]);
                 
                 let listData=[];
                 let locArray=[];
                 if(objfind[0])
                 locArray=objfind[0].location;
                 for(let i=0;i<locArray.length;i++){
                     let objCount=this.state.filteredData.filter((data)=>{
                         return data.visitorData.location===locArray[i]
                     })
                     let obj={};
                     obj.activation=locArray[i];
                     obj.total=objCount.length;
                     listData.push(obj);

                 }
                 this.setState({
                    filteredgraphData:listData,
                    maxValue:Math.max.apply(Math,listData.map(function(o) { return o.total; }))

                 })
            }
            else{
                let object=[];
                let obj={};
                obj.activation=this.state.location;
                obj.total=this.state.filteredData.length;
                object.push(obj);
                this.setState({
                    filteredgraphData:object,
                    maxValue:Math.max.apply(Math,object.map(function(o) { return o.total; }))

                })
            }
            }
            else{
                let updatedObj=this.state.graphData.map((obj)=>{
                    let objcount=this.state.filteredData.filter((data)=>{
                        return data.visitorData.activation===obj.activation
                    })
                    obj.total=objcount.length;
                    return obj;
                })
                this.setState({
                    filteredgraphData:updatedObj,
                    maxValue:Math.max.apply(Math,updatedObj.map(function(o) { return o.total; }))

                })
                console.log(this.state.graphData,updatedObj);
            }
        }
        console.log("change",this.state.filteredgraphData);
    }

    getFilteredDate(activation, location) {
        console.log(activation, location);
        let body = {}
        if (activation && location) {
            body.activation = activation;
            body.location = location;
        }
        else if (activation) {
            body.activation = activation;
        }
        request
            .post(this.apiLink + "oc/activation/filteredData")
            .send(body)
            .then(res => {
                if(!activation && !location){
                    console.log('store')
                    this.setState({
                        registeredData:res.body.dataList
                    })
                }
                console.log(res.body);
                let data = res.body.dataList;
                if (this.mindate !== '' && this.maxdate !== '') {
                    console.log(moment(data[0].sessionTime).format('YYYY-MM-DD'))
                    let filtData = data.filter((data1) => {
                        return (
                            (moment(data1.sessionTime).format('YYYY-MM-DD') >= this.mindate
                                && moment(data1.sessionTime).format('YYYY-MM-DD') <= this.maxdate)
                        )
                    });
                    console.log(filtData);
                    this.setState({
                        filteredData: filtData,
                        
                    })
                }
                else {
                    this.setState({
                        filteredData: res.body.dataList,
                    })
                }
                this.updateGraphData();
            })
            .catch(e => {
                console.log(e);
            });
    }
    getFilteredGraph(){
        request
        .get(this.apiLink + "oc/activation/locations")
        .then(res => {
            console.log(res.body.message.vanNames);
            if(res){
                let locationArray=res.body.message.vanNames;
                request
                .get(this.apiLink + "oc/activation/analytics")
                .then(res => {
                    console.log(res.body.message);
                    let graph=res.body.message;
                    for(let i=0;i<locationArray.length;i++){
                        if(!res.body.message.some(el=>el.activation===locationArray[i])){
                            request
                            .post(this.apiLink + "oc/activation/getLocations")
                            .send({activationName:locationArray[i]})
                            .then(res => {
                                let obj={};
                                obj.total=0;
                                obj.activation=locationArray[i];
                                obj.location=res.body.message;
                                graph.push(obj)
                                // this.setState({
                                //     campaign:res.body[0],
                                //     pageView: "gallery",
                                // })
                            })
                            .catch(e => {
                                console.log(e);
                            }); 
                            }
                        else{
                            request
                            .post(this.apiLink + "oc/activation/getLocations")
                            .send({activationName:locationArray[i]})
                            .then(res => {
                                graph.forEach((element, index) => {
                                    if(element.activation === locationArray[i]) {
                                         element.location = res.body.message;
                                    }
                                });
                               
                            })
                            .catch(e => {
                                console.log(e);
                            }); 
                        }
                    }
                    console.log(graph);
                    let filtered=[];
                    setTimeout( () => {
                         filtered= graph.sort(function(a, b) {
                             if (a.activation && b.activation){
                            var textA = a.activation.toUpperCase();
                            var textB = b.activation.toUpperCase();
                            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                         }
                        });
                        }, 900);
                 
                    console.log(filtered);
                    // graph.sort(function(a, b) {
                    //     return a.activation.localeCompare(b.activation);
                    //  });
                    console.log(graph);
                    setTimeout( () => {
                        this.setState({
                            graphData: filtered,
                            maxValue:Math.max.apply(Math, res.body.message.map(function(o) { return o.total; }))
                        })
                        }, 1000);
                        console.log(this.state.graphData);
                  
                })
                .catch(e => {
                    console.log(e);
                });
            }
        })
        .catch(e => {
            console.log(e);
        });
       
    }
    componentWillMount() {

    }
    componentDidMount() {
        $(function() { 
            $("#clickit").click(function() { 
                html2canvas($("#widget"), {
                    onrendered: function(canvas) {
                        // theCanvas = canvas;
                        document.body.appendChild(canvas);
                         console.log(canvas);
                        // Convert and download as image 
                        // Canvas2Image.saveAsPNG(canvas); 
                        // $("#img-out").append(canvas);
                        // Clean up 
                        //document.body.removeChild(canvas);
                    }
                });
            });
        }); 

        console.log(this.props.match.params.id);
        console.log(window.location.href.split('/')[4]);
        if(window.location.href.split('/')[3]==='yourmovie' && !this.props.match.params.id){
            console.log('llllllllll');
            this.setState({
                pageType:'analytics',
                pageView:"analytics",
            })
            this.props.setlandingpage("analytics");
        }
        if(this.props.match.params.id){
            if(window.location.href.split('/')[3]==='record'){
                this.props.setlandingpage("gallery");
            }
            request
            .post(this.apiLink + "oc/activation/GetMovieVisitor")
            .send({shortcode:this.props.match.params.id})
            .then(res => {
                this.setState({
                    campaign:res.body[0],
                    pageView: "gallery",
                })
            })
            .catch(e => {
                console.log(e);
            }); 

        }
        else if(window.location.href.split('/')[4]==='gallery'){
            this.setState({
                pageView:"analytics",
                pageType:"gallery",
                mobile: "",
                activation: "all",
                location: "all",
                totalCampaigns: 10,
            })
            this.props.setlandingpage("gallery");

        }
        else{
            this.setState({
                pageView:"analytics"
            })
            let token=localStorage.getItem("id_token")
            if (!token) {
                localStorage.removeItem("id_token");
                this.props.history.push({
                  pathname: "/advlogin"
                });
              }
        }
      if(window.location.href.split('/')[3]==='yourmovie' && this.props.match.params.id){

      }
      else{
        this.getFilteredGraph();
        this.getFilteredDate();
      }
      
    }
    componentDidUpdate() {
        let  maxValue  = this.state.maxValue;
    
        $(".horizontal .progress-fill span").each(function() {
          var percent = $(this).html();
          $(this)
            .parent()
            .css("width", percent);
        });
    
        $(".engagementsBar .progress-fill span").each(function() {
          var percent = $(this).html();
          if (percent == 0) {
            //  var pTop = 100 - percent.slice(0, percent.length - 1) + "%";
            $(this)
              .parent()
              .css({
                height: 0,
                bottom: 0
              });
          } else {
            //  var pTop = 100 - percent.slice(0, percent.length - 1) + "%";
            $(this)
              .parent()
              .css({
                height: (percent / maxValue) * 300,
                bottom: 0
              });
          }
        });
      }
    render() {
        return (
            <div id="widget">
                <Header onSelectLanguage={this.handleLanguage} pageType="yourmovie" analytics={this.state.analytics} />
               {this.state.pageView && this.state.pageView==='analytics' && (
                <div className="main">
                    <div className="main-top" ref={this.main}>
                        {this.state.pageType && this.state.pageType === 'gallery' && (
                        <div className="search-gal">
                            <input 
                                name="search" 
                                placeholder="Search by Phone Number" 
                                type="number" autoComplete="off"
                                onChange={this.mobileNumberSearch}
                                value={this.state.mobile}
                                 />
                            <button className="search-click"><img src="/assets/images/searchicon-l.png" alt="" /></button>
                        </div>
                        )}
                        {this.state.pageType && this.state.pageType === 'analytics' && (
                            <div className="title">Engagements</div>
                        )}
                        <div className="filters">
                            <div className="filters-in">
                                <div className="activations select">
                                    <select onChange={this.UpdateActivationFilter} value={this.state.activation} className={this.state.activation === 'all' ? "selectOpt all" : "selectOpt"} required>
                                        <option value="all">All Activations</option>
                                        {this.state.graphData.map((van, index) => (<option value={van.activation} key={index}>{van.activation}</option>))}
                                    </select>
                                </div>
                                <div className="locations select">
                                    <select onChange={this.UpdateLocationFilter} value={this.state.location} className={this.state.location === 'all' ? "selectOpt all" : "selectOpt"} required>
                                        <option value="all">All Locations</option>
                                        {this.state.activation !== 'all' && this.state.locationData.map((van, index) => (<option value={van} key={index}>{van}</option>))}
                                    </select>
                                </div>

                                <div className="date">
                                    <input
                                        type="date"
                                        placeholder="From"
                                        name="fromdate"
                                        onChange={this.UpdateDateOC}
                                        value={this.mindate}
                                        max={this.today}
                                        id="fromdate" required />
                                    <input
                                        type="date"
                                        placeholder="To"
                                        name="todate"
                                        onChange={this.UpdateDateOC}
                                        value={this.maxdate}
                                        min={this.mindate}
                                        // max={this.today}
                                        id="todate" required />
                                </div>
                            </div>
                        </div>
                        {this.state.pageType && this.state.pageType === 'analytics' && (
                            <div className="total">
                                <div className="title">TOTAL
                                {
                                        this.state.filteredData &&
                                                <span className="count" title="Completed">
                                                    {this.state.filteredData.length}
                                                </span>
                                            }
                                </div>
                                <div className="total-values">
                                    <a onClick={(e)=>this.handleExport(e,this.state.filteredData)} className="export">
                                        <img src="assets/images/export.png" /> Export
                                </a>
                                </div>
                            </div>
                        )}

                    </div>
                    {this.state.pageType && this.state.pageType === 'gallery' && (
                        <div className="gallery-main">
                            { this.state.filteredData && this.state.filteredData.slice(0,this.state.count).map((record, index) => (
                                <div key={index} className="yourmve">
                                <div className="img-block"
                                    key={index}
                                    onClick={(e) => this.detail(e, record)}
                                >
                                    <div className="img-block-in" >
                                        <div className="campaign-block">
                                            <div className="activation-details">
                                                <span className="act-date">{moment(record.sessionTime).format('YYYY-MM-DD')}</span>
                                                <span className="act-time">{" " + moment(record.sessionTime).format("HH:mm")} </span>
                                                <span className="act-area">{record.visitorData.location}</span>
                                                <span className="act-num">{record.visitorData.activation}</span>
                                            </div>
                                        </div>
                                        {<div className="campaign-img">
                                            <img src={record.visitorData.picture} />
                                        </div>}

                                    </div>
                                </div>
                                { index+1==this.state.count &&  <div className="loadimg">
                     {/* { this.renderCards() } */}
                     <InfiniteLoader onVisited={ () => this.handleVisit() } />
                   </div>}
                                </div>
                            ))}
                           


                        </div>
                    )}
                    {this.state.pageType && this.state.pageType === 'analytics' && (
                        <div className="analytics-main">
                            <button id="clickit">click</button>
                            <div className="engagementsBar">
                                {this.state.activation==='all' && this.state.location==="all" && (this.mindate==='' || this.maxdate==='') && (
                                    <div className="engagementsBar-in">
                                        <div className="eng-txt">No of Engagements</div>
                                        {this.state.graphData && this.state.graphData.map((graph, index) => (
                                            <div key={index} className="progress-bar" >
                                                <div className="progress-bar-in">
                                                    <div className="progress-track">
                                                        <div className="progress-fill">
                                                            <span >{graph.total}</span>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="bar-name">
                                                    <span className="eng-no">{graph.activation}</span>
                                                </div>
                                            </div>
                                        ))}

                                    </div>
                                )}
                                 {(this.state.activation!=='all'  || (this.mindate!=='' && this.maxdate!=='') )&& (
                                    <div className="engagementsBar-in">
                                        <div className="eng-txt">No of Engagements</div>
                                        {this.state.filteredgraphData && this.state.filteredgraphData.map((graph, index) => (
                                            <div key={index} className="progress-bar" >
                                                <div className="progress-bar-in">
                                                    <div className="progress-track">
                                                        <div className="progress-fill">
                                                            <span>{graph.total}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="bar-name">
                                                    <span className="eng-no">{graph.activation}</span>
                                                </div>
                                            </div>
                                        ))}

                                    </div>
                                )}
                               
                            </div>

                            {/* {this.state.smssent && (
                        <div className="show">SMS sent to the mobile number</div>
                        )}  */}
                            <div className="eng-details">
                                {/* {this.state.filteredData && this.state.filteredData.map((record,index)=> ) */}
                                <table className="details-table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Number</th>
                                            <th>Activation</th>
                                            <th>Location</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Capture</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                        {this.state.filteredData && this.state.filteredData.slice(0, this.state.totalCampaigns).map((record, index) => (
                                            <tr key={index} onClick={(e) => this.detail(e, record)}>
                                                <td data-th="Name">{record.visitorData.name}</td>
                                                <td data-th="Email">{record.visitorData.email} </td>
                                                <td data-th="Number">{record.visitorData.mobile}</td>
                                                <td data-th="Activation">  {record.visitorData.activation}</td>
                                                <td data-th="Location">{record.visitorData.location}</td>
                                                <td data-th="Date">{moment(record.sessionTime).format('YYYY-MM-DD')}</td>
                                                <td data-th="Time">{moment(record.sessionTime).format("HH:mm")}</td>
                                                <td className="download" data-th="Capture"><a onClick={(e) => {e.stopPropagation(); this.picture(e, record.visitorData.picture) }
                                                } ><img src="assets/images/download.png" /></a></td>
                                                {index + 1 == this.state.totalCampaigns && <td className="loadimg">
                                <InfiniteLoader onVisited={(e) => {this.handleData(e,this.state.totalCampaigns)} } />
                            </td>}
                            {index + 1 !== this.state.totalCampaigns && <td className="loadimg">
                                 
                            </td>}
                                            </tr>
                                        ))}


                                    </tbody>
                                </table>

                            </div>
                        </div>
                    )}

                </div>
               )} 
               {this.state.pageView && this.state.pageView==='gallery'  && this.state.campaign && this.state.campaign.visitorData && (
                <div className="main">
                    <div className="act-details-main">
                        <div className="act-details-top">
                            <span className="act-no">{this.state.campaign.visitorData.activation}</span>
                            <span className="act-date">{moment(this.state.campaign.sessionTime).format('YYYY-MM-DD')}</span>
                            <span className="act-time">{" "+moment(this.state.campaign.sessionTime).format("HH:mm")}</span>
                            <span className="act-place">{" "+this.state.campaign.visitorData.location}</span>
                        </div>
                        {this.state.campaign && this.state.campaign.visitorData && this.state.campaign.visitorData.picture &&
                          <div className="act-img">
                          <img src={this.state.campaign.visitorData.picture} />
                      </div>}
                      
                        <div className="act-details-btm">
                            <div className="act-details-btm-left">
                                <span className="name">{this.state.campaign.visitorData.name}</span>
                                <span className="phone">{this.state.campaign.visitorData.mobile}</span>
                            </div>
                            <div className="act-details-btm-right">
                                <a className="download" onClick={(e)=>{ e.stopPropagation();this.picture(e,this.state.campaign.visitorData.picture)}}><img src="/assets/images/downloadImg.png" /></a>
                            </div>
                        </div>
                        {this.state.smssent && <div className='show'>SMS sent to the mobile number</div>}
                    </div>

                  
                </div>
               )}
            </div>

        );
    }
}

function mapStateToProps(state) {
    return {
        logout: state.activeFilter.logout,
        landingpage:state.activeFilter.landingpage
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        setlandingpage:setlandingpage
    }, dispatch);
}

export default withRouter(connect(mapStateToProps, matchDispatchToProps)(Yourmovie));