import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import {
    setnewcampaigns,
    setnextcampaigns,
    activation,
    range,
    setcampainstree,
    setfromtime,
    settotime,
    setfromdate,
    setgender,setLocation,
    settodate, settotalcount, setregisters,setlocationtree, setregistersLoc, setwindowmaxvalue, initregisters
} from '../../actions/index';
import PropTypes from 'prop-types';
const request = require('superagent');

class AdvanceFilters extends Component {
    constructor(props) {
        super(props);
        //  // console.log(this.props)
        this.state = {
            cities: [],
            location: [],
            vanLocation: 'all',
            gender: 'unknown',
            city: 'all',
            fromDate:'',
            toDate:'',
            setValue:'',
        }
        this.apiLink=process.env.REACT_APP_SECRET_CODE.trim();
        this.gender = 'unknown';
        this.maxDate = moment().format('YYYY-MM-DD');
        this.minDate = '';
        this.today = moment().format('YYYY-MM-DD');
        this.UpdateDateFilter = this.UpdateDateFilter.bind(this);
        this.UpdateActivationFilter = this.UpdateActivationFilter.bind(this);
        this.UpdateDateTimeFilter = this.UpdateDateTimeFilter.bind(this);
        this.UpdateDateOC =this.UpdateDateOC.bind(this);
        this.UpdateLocationFilter = this.UpdateLocationFilter.bind(this);
        this.durable = 'all';
        this.location = 'all';
        this.fromDate='';
        this.toData='';

    }


    UpdateActivationFilter(e) {
        let { campaigns,
            activation,
            totalcount,
            setnewcampaigns,
            setnextcampaigns,
            registers,
            setregisters,
            setgender,
            settotalcount } = this.props;
        //  // console.log(this.props);
        let cityName = e.target.value;
        this.setState({
            city: cityName
        })
        // this.props.vanLocation='all';
        this.props.setLocation('all');

        this.location = 'all';
        this.durable = 'all';
        this.setState({
            vanLocation: 'all'
        })
        if (cityName == 'all') {

            this.gender = 'unknown'
            this.props.onSelectGender('unknown');
            this.props.datetimerangeSelected('all')
            // console.log(this.state.city);

        }

        request
            .post(this.apiLink+'oc/activation/getLocations')
            .send({ "activationName": cityName })
            .then(res => {
                //  // console.log('location:',res.text)     
                let location = res.body.message;
                // let newLocation = location.join(",").split(",");
                let uniqueLocation = location.filter((v, i, a) => a.indexOf(v) === i);

                if (cityName !== 'all') {
                    this.setState({
                        location: [
                            ...uniqueLocation
                        ]
                    })
                }
                else {
                    this.setState({
                        location: [

                        ]
                    })
                }


                let locationArray = uniqueLocation;
                activation(cityName);



                let filterCampaigns = campaigns.filter((campaign) => {
                    if (cityName !== 'all') {
                        return cityName===campaign.visitorData.activation;
                    }
                    else {
                        return campaign;
                    }
                });
               

                //  // console.log(filter2);
                if (cityName !== 'all') {
                    let newRegisters = [];
                    let countLoc = 0;

                    locationArray.map((data) => {
                        // // console.log(this.props);
                        let { initregistersLoc } = this.props;
                        // if (this.gender == 'unknown') {
                            if (initregistersLoc[data] !== undefined) {
                                countLoc = countLoc + initregistersLoc[data].length;
                            }
                            if (this.props.campainsByCity[cityName])
                                newRegisters[data] = this.props.campainsByCity[cityName][data];
                        // }
                        // else {
                        //     let filtered = [];
                        //     var gend = this.gender;
                        //     if (this.props.campainsByCity && this.props.campainsByCity[cityName][data])
                        //         filtered = this.props.campainsByCity[cityName][data].filter(function (value, index, arr) {
                        //             if (value == gend) {
                        //                 return value;

                        //             }

                        //         });
                        //     newRegisters[data] = filtered;
                        // }

                    });
                    // // console.log(this.props);
                    settotalcount(filterCampaigns.length)
                    setregisters(newRegisters)
                    setnewcampaigns(filterCampaigns);
                    setnextcampaigns(filterCampaigns);

                } else {
                    this.setState({ gender: 'unknown' });

                    settotalcount(campaigns.length);
                    setregisters(this.props.initregisters);
                    setnewcampaigns(filterCampaigns);
                    setnextcampaigns(filterCampaigns);


                }
            });
                let locationTree={};
                campaigns.map((campaign)=>{
                    let activation = campaign.visitorData.activation;
                    let location = campaign.visitorData.location;

                    if(locationTree[activation]===undefined){
                        locationTree[activation] = {};
                        locationTree[activation][location] = 1;
                        // locationTree[activation][location].count=1;
                      }
                      else if (locationTree[activation][location] === undefined) {
                        locationTree[activation][location] = 1;
                        // locationTree[activation][location].count=1;
                      } else {
                        locationTree[activation][location]+=1;
                      }
                })
                this.props.setlocationtree(locationTree);
        this.props.vanSelected(e.target.value);

    }

    async UpdateDateFilter(e) {
        let { range,
            campaigns,
            activation,
            totalcount,
            setnewcampaigns,
            setnextcampaigns,
            registers,
            actvalue,
            newcampaigns,
            nextcampaigns } = this.props;

        this.props.datetimerangeSelected(e.target.value);
        this.durable = e.target.value;
        let duration = e.target.value;
        await range(duration);

        const dateTo = moment().format('YYYY-MM-DD');
        const weeklyFrom = moment().subtract(7, 'd').format('YYYY-MM-DD');
        const monthlyFrom = moment().subtract(30, 'd').format('YYYY-MM-DD');

        let durationCampaigns = (ccampaigns) => {
            return ccampaigns.filter((campaign) => {
                if (duration !== 'all') {
                    if (duration === 'today') {
                        return moment(campaign.visitorData.date_time).format('YYYY-MM-DD') === dateTo
                    } else if (duration === 'weekly') {
                        return moment(campaign.visitorData.date_time).format('YYYY-MM-DD') >= weeklyFrom && moment(campaign.visitorData.date_time).format('YYYY-MM-DD') <= dateTo
                    } else if (duration === 'monthly') {
                        return moment(campaign.visitorData.date_time).format('YYYY-MM-DD') >= monthlyFrom && moment(campaign.visitorData.date_time).format('YYYY-MM-DD') <= dateTo
                    }
                } else {
                    return campaign;
                }
            })
        }

        let dCampaigns;
        // // console.log(this.props);
        if (this.props.actvalue !== 'all') {
            if (this.props.rangevalue !== 'all') {
                dCampaigns = durationCampaigns(newcampaigns);
                await setnextcampaigns(dCampaigns);
            } else {
                dCampaigns = durationCampaigns(newcampaigns);
                await setnewcampaigns(dCampaigns);
            }
        } else if (this.props.actvalue === 'all') {
            if (this.props.rangevalue !== 'all') {
                dCampaigns = durationCampaigns(newcampaigns);
                await setnextcampaigns(dCampaigns);
            } else {
                dCampaigns = durationCampaigns(campaigns);
                await setnewcampaigns(dCampaigns);
            }
        }


        let nregisters = [];
        if (this.props.actvalue == 'all') {
            for (let campaign of dCampaigns) {
                if (campaign.visitorData.activation !== undefined) {
                    let activation = campaign.visitorData.activation;
                    if (nregisters[activation] === undefined) {
                        nregisters[activation] = [];
                        nregisters[activation].push(campaign.visitorData)
                    } else {
                        nregisters[activation].push(campaign.visitorData)
                    }
                }
            }
            await this.props.setregisters(nregisters);

        }
        else if (this.props.actvalue !== 'all' && this.state.vanLocation == 'all') {
            for (let campaign of dCampaigns) {
                if (campaign.visitorData.activation == this.props.actvalue) {
                    if (campaign.visitorData.activation !== undefined) {
                        let activation = campaign.visitorData.activation;
                        if (nregisters[activation] === undefined) {
                            nregisters[activation] = [];
                            nregisters[activation].push(campaign.visitorData)
                        } else {
                            nregisters[activation].push(campaign.visitorData)
                        }
                    }
                }

            }
            let newRegisters = [];
            this.state.location.map((data) => {
                newRegisters[data] = this.props.initregistersLoc[data];
            });



            await this.props.setregisters(nregisters);


        } else if (this.props.actvalue !== 'all' && this.state.vanLocation !== 'all') {
            for (let campaign of dCampaigns) {
                if (campaign.visitorData.activation == this.props.actvalue) {
                    if (campaign.visitorData.activation !== undefined) {
                        let activation = campaign.visitorData.activation;
                        if (nregisters[activation] === undefined) {
                            nregisters[activation] = [];
                            nregisters[activation].push(campaign.visitorData)
                        } else {
                            nregisters[activation].push(campaign.visitorData)
                        }
                    }
                }

            }
            await this.props.setregisters(nregisters);

        }

        let totalCount = 0;
        let activations = Object.keys(nregisters);
        for (let activation of activations) {
            totalCount = totalCount + nregisters[activation].length
        }

        await this.props.settotalcount(totalCount)



        // actvalue !== 'all' ? setnewcampaigns(durationCampaigns(nextcampaigns)) : setnewcampaigns(durationCampaigns(campaigns));
    }


    UpdateDateOC(e){
        const value=e.target.value;
        const name=e.target.name;
        if (name == 'fromdate') {
           
            this.minDate = value;
             this.props.setfromdate(value)
        } else {
           this.maxDate=value;
             this.props.settodate(value)
        } 
        console.log(this.minDate,this.maxDate);
        if(this.minDate!=='' && this.maxDate!==''){
            const dateFrom = moment(this.props.fromdate).format('YYYY-MM-DD');
            const dateTo = moment(this.props.todate).format('YYYY-MM-DD');
            console.log(this.props.location);
            if (this.props.actvalue === 'all') {
                let { campaigns } = this.props;
                    let range = campaigns.filter((campaign) => {
                        return (
                            (moment(campaign.sessionTime).format('YYYY-MM-DD') >= this.minDate
                                && moment(campaign.sessionTime).format('YYYY-MM-DD') <= this.maxDate)
                        )
                    })
                    console.log(range);
                    let registers = [];
                    for (let campaign of range) {
                        if (campaign.visitorData.activation !== undefined) {
                            let activation = campaign.visitorData.activation;
                            if (registers[activation] === undefined) {
                                registers[activation] = [];
                                registers[activation].push(campaign.visitorData)
                            } else {
                                registers[activation].push(campaign.visitorData)
                            }
                        }
                    }

                    this.props.setregisters(registers);
                    let totalCount = 0;
                    let activations = Object.keys(registers);
                    for (let activation of activations) {
                        totalCount = totalCount + registers[activation].length
                    }

                    this.props.setnextcampaigns(range)
                    this.props.settotalcount(totalCount)


                
            }
        else if(this.props.actvalue !== 'all' && this.props.location==='all'){
            let { campaigns } = this.props;
            let locationTree={};
            console.log(campaigns);
            let newcampaigns=campaigns.filter((campaign)=>{
                return(campaign.visitorData.activation===this.props.actvalue)
            })
            let range = newcampaigns.filter((campaign) => {
                return (
                    (moment(campaign.sessionTime).format('YYYY-MM-DD') >= this.minDate
                        && moment(campaign.sessionTime).format('YYYY-MM-DD') <= this.maxDate)
                )
            })
            console.log(range);
            range.map((campaign)=>{
                let activation = campaign.visitorData.activation;
                let location = campaign.visitorData.location;

                if(locationTree[activation]===undefined){
                    locationTree[activation] = {};
                    locationTree[activation][location] = 1;
                    // locationTree[activation][location].count=1;
                  }
                  else if (locationTree[activation][location] === undefined) {
                    locationTree[activation][location] = 1;
                    // locationTree[activation][location].count=1;
                  } else {
                    locationTree[activation][location]+=1;
                  }
            })
            this.props.setlocationtree(locationTree);

            let registers = [];
            for (let campaign of range) {
                if (campaign.visitorData.activation !== undefined) {
                    let activation = campaign.visitorData.activation;
                    if (registers[activation] === undefined) {
                        registers[activation] = [];
                        registers[activation].push(campaign.visitorData)
                    } else {
                        registers[activation].push(campaign.visitorData)
                    }
                }
            }

            this.props.setregisters(registers);
            let totalCount = 0;
            let activations = Object.keys(registers);
            for (let activation of activations) {
                totalCount = totalCount + registers[activation].length
            }

            this.props.setnextcampaigns(range)
            this.props.settotalcount(totalCount)

        }
    else{
        let { campaigns } = this.props;
        console.log(campaigns);
        let newcampaigns=campaigns.filter((campaign)=>{
            return(campaign.visitorData.activation===this.props.actvalue)
        })
        let new2Camp=newcampaigns.filter((campaign)=>{
            return(campaign.visitorData.location===this.props.location)

        })
        let range = new2Camp.filter((campaign) => {
            return (
                (moment(campaign.sessionTime).format('YYYY-MM-DD') >= this.minDate
                    && moment(campaign.sessionTime).format('YYYY-MM-DD') <= this.maxDate)
                // && (moment(campaign.visitorData.date_time).format('HH:MM') >= this.props.fromtime 
                // && moment(campaign.visitorData.date_time).format('HH:MM') <= this.props.totime)
            )
        })
        console.log(range);
        let locationTree={};
        range.map((campaign)=>{
            let activation = campaign.visitorData.activation;
            let location = campaign.visitorData.location;

            if(locationTree[activation]===undefined){
                locationTree[activation] = {};
                locationTree[activation][location] = 1;
                // locationTree[activation][location].count=1;
              }
              else if (locationTree[activation][location] === undefined) {
                locationTree[activation][location] = 1;
                // locationTree[activation][location].count=1;
              } else {
                locationTree[activation][location]+=1;
              }
        })
        this.props.setlocationtree(locationTree);
        let registers = [];
        for (let campaign of range) {
            if (campaign.visitorData.activation !== undefined) {
                let activation = campaign.visitorData.activation;
                if (registers[activation] === undefined) {
                    registers[activation] = [];
                    registers[activation].push(campaign.visitorData)
                } else {
                    registers[activation].push(campaign.visitorData)
                }
            }
        }

        this.props.setregisters(registers);
        let totalCount = 0;
        let activations = Object.keys(registers);
        for (let activation of activations) {
            totalCount = totalCount + registers[activation].length
        }

        this.props.setnextcampaigns(range)
        this.props.settotalcount(totalCount)
    }        }
    }
     UpdateDateTimeFilter(e) {
        const value = e.target.value;
        const name = e.target.name;
        console.log(value,name);
        if (name == 'fromdate') {
            this.setState({
                fromDate:value
            })
            this.minDate = value;
             this.props.setfromdate(value)
        } else {
            this.toDate=value;
            this.setState({
                toDate:value
            })
             this.props.settodate(value)
        } 
        this.setState({
            setValue:name
        })
        if (this.state.setValue!=='' && this.state.setValue!==name) {
        
        this.props.datetimerangeSelected(e.target.value);
     
        console.log(this.state.setValue)
        
            this.durable = 'all'
            const dateFrom = moment(this.props.fromdate).format('YYYY-MM-DD');
            const dateTo = moment(this.props.todate).format('YYYY-MM-DD');


            if (this.props.actvalue === 'all') {
                let { campaigns } = this.props;
                    let range = campaigns.filter((campaign) => {
                        return (
                            (moment(campaign.visitorData.date_time).format('YYYY-MM-DD') >= dateFrom
                                && moment(campaign.visitorData.date_time).format('YYYY-MM-DD') <= dateTo)
                        )
                    })
                    let registers = [];
                    for (let campaign of range) {
                        if (campaign.visitorData.activation !== undefined) {
                            let activation = campaign.visitorData.activation;
                            if (registers[activation] === undefined) {
                                registers[activation] = [];
                                registers[activation].push(campaign.visitorData)
                            } else {
                                registers[activation].push(campaign.visitorData)
                            }
                        }
                    }

                    this.props.setregisters(registers);
                    let totalCount = 0;
                    let activations = Object.keys(registers);
                    for (let activation of activations) {
                        totalCount = totalCount + registers[activation].length
                    }

                    this.props.setnextcampaigns(range)
                    this.props.settotalcount(totalCount)


                
            } else {
                let { newcampaigns } = this.props;
                    let range = newcampaigns.filter((campaign) => {
                        return (
                            (moment(campaign.visitorData.date_time).format('YYYY-MM-DD') >= dateFrom
                                && moment(campaign.visitorData.date_time).format('YYYY-MM-DD') <= dateTo)
                            // && (moment(campaign.visitorData.date_time).format('HH:MM') >= this.props.fromtime 
                            // && moment(campaign.visitorData.date_time).format('HH:MM') <= this.props.totime)
                        )
                    })
                    let registers = [];
                    for (let campaign of range) {
                        if (campaign.visitorData.activation !== undefined) {
                            let activation = campaign.visitorData.activation;
                            if (registers[activation] === undefined) {
                                registers[activation] = [];
                                registers[activation].push(campaign.visitorData)
                            } else {
                                registers[activation].push(campaign.visitorData)
                            }
                        }
                    }

                    this.props.setregisters(registers);
                    let totalCount = 0;
                    let activations = Object.keys(registers);
                    for (let activation of activations) {
                        totalCount = totalCount + registers[activation].length
                    }

                    this.props.setnextcampaigns(range)
                    this.props.settotalcount(totalCount)

                 
            }

        }
    }

    UpdateLocationFilter(e) {
        let { campaigns,
            activation,
            totalcount,
            setnewcampaigns,
            setnextcampaigns,
            registers,
            actvalue,
            setregisters,
            settotalcount,
            setLocation } = this.props;
        this.props.setLocation(e.target.value);
        let location = e.target.value;
        this.props.settodate('')
        this.props.setfromdate('')

        // this.props.datetimerangeSelected(e.target.value);
        let cityName = actvalue;
        this.durable = 'all';
        this.maxDate = moment().format('YYYY-MM-DD');
        this.minDate = '';
        this.props.datetimerangeSelected('all')

        //let locationArray = Visitors[cityName] !== undefined ? Object.keys(Visitors[cityName]) : ''; 
        let locationArray = this.state.location;

        this.setState({
            vanLocation: e.target.value
        });
        this.location = e.target.value;

        let filterCampaigns = campaigns.filter((campaign) => {
            if (e.target.value !== 'all') {

                return campaign.visitorData.location === e.target.value ? campaign : ''
            }
            else {

                return campaign;
            }

        });

        if (actvalue !== 'all' && location !== 'all') {
            // console.log(actvalue,location);
            let locationTree={};
            campaigns.map((campaign)=>{
                let activation = campaign.visitorData.activation;
                let location = campaign.visitorData.location;

                if(locationTree[activation]===undefined){
                    locationTree[activation] = {};
                    locationTree[activation][location] = 1;
                    // locationTree[activation][location].count=1;
                  }
                  else if (locationTree[activation][location] === undefined) {
                    locationTree[activation][location] = 1;
                    // locationTree[activation][location].count=1;
                  } else {
                    locationTree[activation][location]+=1;
                  }
            })
            this.props.setlocationtree(locationTree);
            let newRegisters = [];
            if (this.props.campainsByCity && this.props.campainsByCity[actvalue] && this.props.campainsByCity[actvalue][location])
                newRegisters[location] = this.props.campainsByCity[actvalue][location].filter((value) => {                   
                        return value;
                     });
            if (newRegisters[location]) {

                settotalcount(this.props.locationtree[actvalue][location] ? this.props.locationtree[actvalue][location] : 0)

            }
            else {
                settotalcount(0);
            }

            setregisters(newRegisters)
            setnewcampaigns(filterCampaigns);
            setnextcampaigns(filterCampaigns);


        } else if (actvalue !== 'all' && location === 'all') {
            this.setState({ gender: 'all' });

            let filterCampaigns = campaigns.filter((campaign) => {
                if (cityName !== 'all') {
                    return locationArray.indexOf(campaign.visitorData.location) > -1 ? campaign : ''
                } else {
                    return campaign;
                }
            });

            let newRegisters = [];
            let countLoc = 0;

            locationArray.map((data) => {
                countLoc = countLoc + this.props.campainsByCity[actvalue][data] ? this.props.campainsByCity[actvalue][data].length : 0;
                if (this.props.campainsByCity && this.props.campainsByCity[actvalue][data])
                    newRegisters[data] = this.props.campainsByCity[actvalue][data].filter((value) => {
                        if (this.gender !== 'unknown') {
                            return value == this.gender;

                        }
                        else {
                            return value;
                        }
                    });
            });
            let locationTree={};
            filterCampaigns.map((campaign)=>{
                let activation = campaign.visitorData.activation;
                let location = campaign.visitorData.location;

                if(locationTree[activation]===undefined){
                    locationTree[activation] = {};
                    locationTree[activation][location] = 1;
                    // locationTree[activation][location].count=1;
                  }
                  else if (locationTree[activation][location] === undefined) {
                    locationTree[activation][location] = 1;
                    // locationTree[activation][location].count=1;
                  } else {
                    locationTree[activation][location]+=1;
                  }
            })
            this.props.setlocationtree(locationTree);
            settotalcount(filterCampaigns.length)
            setregisters(newRegisters)
            setnewcampaigns(filterCampaigns);
            setnextcampaigns(filterCampaigns);



        } else {
            settotalcount(campaigns.length);
            setregisters(this.props.initregistersLoc);
            setnewcampaigns(filterCampaigns);
            setnextcampaigns(filterCampaigns);


        }
    }




    componentDidMount() {
        request
            .get(this.apiLink+'oc/activation/locations')
            .then(res => {
                let city = res.body.message;
                // console.log('cities:',city.vanNames)
                this.setState({
                    cities: [
                        ...city.vanNames
                    ]
                })
            });

        this.props.vanSelected('all');
    }



    render() {
        let { registers = [] } = this.props;
        let activations = Object.keys(registers);


        return (
            <div className="filters">
                <div className="filters-in">
                    <div className="activations select">
                        <select onChange={this.UpdateActivationFilter} value={this.props.actvalue} className={this.props.actvalue === 'all' ? "selectOpt all" : "selectOpt"} required>
                            <option value="all">All Activations</option>
                            {this.state.cities.map((van, index) => (<option value={van} key={index}>{van}</option>))}
                        </select>
                    </div>
                    <div className="locations select">
                        <select onChange={this.UpdateLocationFilter} value={this.state.vanLocation} className={this.props.vanLocation === 'all' ? "selectOpt all" : "selectOpt"} required>
                            <option value="all">Location</option>
                            {this.state.city !== 'all' && this.state.location.map((van, index) => (<option value={van} key={index}>{van}</option>))}
                        </select>
                    </div>

                    <div className="date">
                        <input
                            type="date"
                            placeholder="From"
                            name="fromdate"
                            onChange={this.UpdateDateOC}
                            value={this.props.fromdate}
                            max={this.maxDate}
                            id="fromdate" required />
                        <input
                            type="date"
                            placeholder="To"
                            name="todate"
                            onChange={this.UpdateDateOC}
                            value={this.props.todate}
                            min={this.minDate}
                            // max={this.today}
                            id="todate" required />
                    </div>
                    {/* <div className="time">
                        <input 
                            type="time" 
                            placeholder="12.10" 
                            name="fromtime" 
                            onChange={this.UpdateDateTimeFilter} 
                            value={this.props.fromtime}
                            id="fromtime" required />
                        <input 
                            type="time" 
                            placeholder="15.15" 
                            name="totime" 
                            onChange={this.UpdateDateTimeFilter} 
                            value={this.props.totime}
                            id="totime" required />
                    </div> */}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {

        actvalue: state.activeFilter.actvalue,
        rangevalue: state.activeFilter.rangevalue,
        fromdate: state.activeFilter.fromDate,
        todate: state.activeFilter.toDate,
        fromtime: state.activeFilter.fromTime,
        totime: state.activeFilter.toTime,
        campaigns: state.activeFilter.campaigns,
        newcampaigns: state.activeFilter.newcampaigns,
        registers: state.activeFilter.registers,
        initregisters: state.activeFilter.initregisters,
        registersLoc: state.activeFilter.registersLoc,
        initregistersLoc: state.activeFilter.initregistersLoc,
        totalcount: state.activeFilter.totalcount,
        nextcampaigns: state.activeFilter.nextcampaigns,
        gender: state.activeFilter.gender,
        campainstree: state.activeFilter.campainstree,
        locationtree:state.activeFilter.locationtree,
        location:state.activeFilter.location
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        setLocation:setLocation,
        setnewcampaigns: setnewcampaigns,
        setnextcampaigns: setnextcampaigns,
        setlocationtree:setlocationtree,
        setregisters: setregisters,
        setregistersLoc: setregistersLoc,
        setwindowmaxvalue: setwindowmaxvalue,
        activation: activation,
        setcampainstree: setcampainstree,
        range: range,
        setfromdate: setfromdate,
        settodate: settodate,
        setfromtime: setfromtime, settotime: settotime, settotalcount: settotalcount
    }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(AdvanceFilters);
