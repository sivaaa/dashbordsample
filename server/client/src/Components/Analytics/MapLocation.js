
function Location(activation,index){
    let location = 'Unknown';
    switch(activation){
        case 'MP1':
            location = (index % 2 === 0) ? 'JNTU' : 'PragathiNagar';
            break;
        case 'MP2':
            location = (index % 2 === 0) ? 'Kondapur' : 'Kphb';
            break;
        case 'MH3':
            location = (index % 2 === 0) ? 'Manikonda' : 'Towlichowki';
            break;
        case 'RAJ1':
            location = (index % 2 === 0) ? 'Lingampalli' : 'Balanagar';
            break;
        case 'RAJ2':
            location = (index % 2 === 0) ? 'Nizampet' : 'Kokapet';
            break;
        case 'RAJ3':
            location = (index % 2 === 0) ? 'Mehadipatnam' : 'Langerhouse';
            break;
        case 'RAJ4':
            location = (index % 2 === 0) ? 'Begumpet' : 'Ameerpet';
            break;
        case 'HAR1':
            location = (index % 2 === 0) ? 'Alwal' : 'Tirumulgiri';
            break;
        case 'PUN1':
            location = (index % 2 === 0) ? 'Paradise' : 'Ranigunj';
            break;
        case 'MH1':
            location = (index % 2 === 0) ? 'Uppal' : 'Bodupal';
            break;
        case 'MH2':
            location = (index % 2 === 0) ? 'Ramnagar' : 'Amberpet';
            break;
        case 'MH4':
            location = (index % 2 === 0) ? 'ECIL' : 'DefenceColony';
            break;
    }
    return location
}
export default Location;