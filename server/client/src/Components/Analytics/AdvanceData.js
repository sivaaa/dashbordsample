//
import React, { Component } from "react";

import Filters from "../Gallery/Filters";
import moment from "moment";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import {
  setlogin,
  setcampaigns,
  setnewcampaigns,
  setnextcampaigns,
  setinitregisters,
  setregisters,
  settotalcount,
  setwindowmaxvalue,
  setinitregistersLoc,
  setregistersLoc,
  setcampainstree,
  setlocationtree,
  setheight,
  activation,
  range,
  setfromtime,
  settotime,
  setfromdate,
  settodate,
  setgender
} from "../../actions/index";
import AuthService from "./../../routers/AuthService";
import withAuth from "./../../routers/withAuth";
import { NavLink, withRouter } from "react-router-dom";
import UpdateActivation from "./UpdateActivation";
import UpdateActivationList from "./UpdateActivationList";
import Header from "../Header/Header";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from "recharts";
import AdvanceFilters from "./AdvanceFilters";
import * as $ from "jquery";
const Auth = new AuthService("http://api.wowso.me/tvs");
const request = require("superagent");

class Analytics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalCampaigns:10,
      analytics: true,
      smssent: false,
      city: "all",
      gender: "unknown",
      gamestatus: {},
      campainbycity: [],
      selectedVan: "all",
      currentlocation: "",
      datetimerange: ""
    };
    //  console.log(this.props);
    this.handleExport = this.handleExport.bind(this);
    this.detail = this.detail.bind(this);
    this.handleSms = this.handleSms.bind(this);
    this.handleVisit=this.handleVisit.bind(this);
    this.loader=this.loader.bind(this);
    this.picture = this.picture.bind(this);
    //  this.loadData = this.loadData.bind(this);
    this.main = React.createRef();
    this.updateGender = this.updateGender.bind(this);
    this.OnGroundData = {
      TIRUPATI: 437,
      HYDERABAD: 656,
      SONEPUR: 319,
      COIMBATORE: 354,
      VIJAYAWADA: 410,
      BERHAMPUR: 428,
      MADURAI: 325,
      RAIPUR: 317,
      BANGALORE: 253,
      HUBLI: 263,
      PONDICHERRY: 368,
      BEHRAMPUR: 428,
      all: 4130
    };
    this.apiLink=process.env.REACT_APP_SECRET_CODE.trim();
  }

  componentWillMount() {
    console.log(process.env.REACT_APP_SECRET_CODE.trim());
    if (this.props.login === false) {
      localStorage.removeItem("id_token");
      this.props.history.push({
        pathname: "/advlogin"
      });
    }
  }
componentDidMount(){
  this.handleVisit();
}
handleVisit() {
  this.setState({
    totalCampaigns:this.state.totalCampaigns+10,
  })
}
  detail(e, campaign) {
    let id = campaign.visitorData.shortcode.split("me/")[1];
    this.props.history.push({
      pathname: `/gallery/${campaign.visitorData.shortcode}`,
      search: "",
      state: { detail: campaign }
    });
  }

  vanSelected = van => {
    //  console.log('new van selected', van);
    this.setState({ selectedVan: van });
  };

  datetimerangeSelected = dtr => {
    console.log("date time range", dtr);
    this.setState({ datetimerange: dtr });
  };

  handleGender = langValue => {
    console.log(langValue);
    this.setState({ gender: langValue });
  };
  updateGender(e) {
    this.setState({
      gender: e.target.value
    });
  }
  handleSms(e,count) {
  
console.log(count);
    setTimeout(() => {
      this.setState({
        totalCampaigns: count+10
      });
    }, 1000);
  }
  loader(e,value){
    console.log(value);
    this.setState({
      totalCampaigns:value+10,
    })
  }
  picture(e, url) {
    var milliseconds = (new Date).getTime();

    // var uniqueKey =
    //   this.props.actvalue +
    //   "_" +
    //   Math.random()
    //     .toString(36)
    //     .substr(2, 5);
    var xhr = new XMLHttpRequest();
    var img = url.replace(/^https:\/\//i, "http://");
    xhr.open("GET", img, true);
    xhr.responseType = "blob";
    xhr.onload = function() {
      var urlCreator = window.URL || window.webkitURL;
      var imageUrl = urlCreator.createObjectURL(this.response);
      var tag = document.createElement("a");
      tag.href = imageUrl;
      tag.download = `${milliseconds}.png`;
      document.body.appendChild(tag);
      tag.click();
      document.body.removeChild(tag);
    };
    xhr.send();
  }
  convertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';

    for (var i = 0; i < array.length; i++) {
      var line = '';
      for (var index in array[i]) {
        if (line != '') line += ','

        line += array[i][index];
      }

      str += line + '\r\n';
    }

    return str;
  }
  exportCSVFile(headers, items, fileTitle) {
    if (headers) {
      items.unshift(headers);
    }

    // Convert Object to JSON
    var jsonObject = JSON.stringify(items);

    var csv = this.convertToCSV(jsonObject);

    var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

    var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
      var link = document.createElement("a");
      if (link.download !== undefined) { // feature detection
        // Browsers that support HTML5 download attribute
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", exportedFilenmae);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }
  handleExport() {
    
    let { campaigns, nextCampaigns, newCampaigns } = this.props;
    let _campaigns;
    if (this.props.actvalue === "all" && this.props.rangevalue === "all") {
      console.log(campaigns);
      if (this.props.fromdate === "" && this.props.todate === "") {
        _campaigns = campaigns;
      } else if (this.props.fromdate !== "" && this.props.todate !== "") {
        _campaigns = nextCampaigns !== undefined ? nextCampaigns : campaigns;
      } else {
        _campaigns = campaigns;
      }
    } else if (
      this.props.actvalue !== "all" &&
      this.props.rangevalue === "all"
    ) {
      _campaigns = nextCampaigns;
    } else if (
      this.props.actvalue === "all" &&
      this.props.rangevalue !== "all"
    ) {
      _campaigns = nextCampaigns;
    } else if (
      this.props.actvalue !== "all" &&
      this.props.rangevalue !== "all"
    ) {
      _campaigns = newCampaigns;
    }
    const itemsFormatted = [];
    
    for(let i=0;i<_campaigns.length;i++){
    let obj={};
    let userData=_campaigns[i].visitorData;
    obj.name=userData.name;
    obj.mobile=userData.mobile;
    obj.email=userData.email;
    obj.activation=userData.activation;
    obj.location=userData.location;
    obj.date=moment(_campaigns[i].sessionTime).format("YYYY-MM-DD");
    obj.time=moment(_campaigns[i].sessionTime).format("HH:mm");
    obj.picture=userData.picture
    itemsFormatted.push(obj);
    }
    console.log(itemsFormatted);
    var headers = {
      name: 'Name'.replace(/,/g, ''), // remove commas to avoid errors
      mobile: "Mobile",
      email: "Email",
      activation: "Activation",
      location: "Location",
      date:"Date",
      time:"time",
      picture: "Picture"
    };
    var fileTitle = 'UserList'; // or 'my-unique-title'

    this.exportCSVFile(headers, itemsFormatted, fileTitle);
    
  }


  componentWillUnmount() {
    // clearInterval(this.myInterval);
  }

  componentDidMount() {
    let { setwindowmaxvalue, settotalcount } = this.props;
    // console.log(settotalcount);
    this.props.activation("all");
    this.props.range("all");
    this.props.setfromtime("");
    this.props.settotime("");
    this.props.setfromdate("");
    this.props.settodate("");

    request
      .get(this.apiLink+"oc/activation/GetVisitors")
      .then(res => {
        let arr = JSON.parse(res.text);
        let registers = {};
        let registersLoc = {};
        let RegistrationCount = {};
        let locationTree={};
        // let cityList = [];
        let campaigns = arr.todos;

        campaigns.map((campaign, index) => {
          //  cityList.push(campaign.visitorData.city);
          if (campaign.visitorData.activation !== undefined) {
            let activation = campaign.visitorData.activation;
            let location = campaign.visitorData.location;


            if(locationTree[activation]===undefined){
              locationTree[activation] = {};
              locationTree[activation][location] = 1;
              // locationTree[activation][location].count=1;
            }
            else if (locationTree[activation][location] === undefined) {
              locationTree[activation][location] = 1;
              // locationTree[activation][location].count=1;
            } else {
              locationTree[activation][location]+=1;
            }
            if (RegistrationCount[activation] === undefined) {
              RegistrationCount[activation] = {};
              RegistrationCount[activation][location] = [];
              RegistrationCount[activation][location].push(
                campaign.visitorData.gender
              );
            } else if (RegistrationCount[activation][location] === undefined) {
              RegistrationCount[activation][location] = [];
              RegistrationCount[activation][location].push(
                campaign.visitorData.gender
              );
            } else {
              RegistrationCount[activation][location].push(
                campaign.visitorData.gender
              );
            }

            if (registers[activation] === undefined) {
              registers[activation] = [];
              registers[activation].push(campaign.visitorData);
            } else {
              registers[activation].push(campaign.visitorData);
            }

            if (registersLoc[location] === undefined) {
              registersLoc[location] = [];
              registersLoc[location].push(campaign.visitorData.gender);
            } else {
              registersLoc[location].push(campaign.visitorData.gender);
            }
          }
        });

        // var uniqueCity = cityList.filter((v, i, a) => a.indexOf(v) === i);
        // console.log(RegistrationCount);
        this.setState({
          campainbycity: RegistrationCount
        });
        //  console.log(this.state.campainbycity);
        console.log(locationTree);
        this.props.setlocationtree(locationTree);
        //  this.props.setcampainstree(RegistrationCount);
         this.props.setcampainstree(RegistrationCount);
       
        this.props.setcampaigns(campaigns);
         this.props.setnewcampaigns(campaigns);
         this.props.setinitregisters(registers);
        this.props.setregisters(registers);
        this.props.setinitregistersLoc(registersLoc);
        this.props.setregistersLoc(registersLoc);

        let maxValue = 0;
        let maxValues = [];
        let keyValues = Object.keys(registers);

        // console.log('what are my keys:',keyValues);

        for (let key of keyValues) {
          maxValues.push(registers[key].length);
        }

      //  console.log("fjffdj", this.state.datetimerange);
        for (let key of keyValues) {
          if (this.OnGroundData[key]) {
            maxValues.push(this.OnGroundData[key]);
          }
        }

        maxValue = Math.max(...maxValues);
        setwindowmaxvalue(maxValue);

        let activations = Object.keys(registers);
        let totalcount = 0;
        for (let activation of activations) {
          totalcount = totalcount + registers[activation].length;
        }

        settotalcount(totalcount);
      })
      .catch(e => {
        console.log(e);
      });
 
  }

  componentDidUpdate() {
    let { maxValue } = this.props;

    $(".horizontal .progress-fill span").each(function() {
      var percent = $(this).html();
      $(this)
        .parent()
        .css("width", percent);
    });

    $(".engagementsBar .progress-fill span").each(function() {
      var percent = $(this).html();
      if (percent == 0) {
        //  var pTop = 100 - percent.slice(0, percent.length - 1) + "%";
        $(this)
          .parent()
          .css({
            height: 0,
            bottom: 0
          });
      } else {
        //  var pTop = 100 - percent.slice(0, percent.length - 1) + "%";
        $(this)
          .parent()
          .css({
            height: (percent / maxValue) * 300,
            bottom: 0
          });
      }
    });
  }

  render() {
    // let CityList = (props) => {
    //     return props.uniqueCity.map((city, index) => {
    //         return (
    //             <option value={city} key={index}>{city}</option>
    //         )
    //     })
    // }

    return (
      <div>
        <Header pageType="exitometer" analytics={this.state.analytics} />
        <div className="main">
          <div className="main-top" ref={this.main}>
            <div className="title">Engagements</div>
            <AdvanceFilters
              vanSelected={this.vanSelected}
              campainsByCity={this.state.campainbycity}
              onSelectGender={this.handleGender}
              datetimerangeSelected={this.datetimerangeSelected}
            />

            <div className="total">
              <div className="title">TOTAL
                {
                  ((this.state.datetimerange === "" ||  this.state.datetimerange === "all") && this.state.gender ==="unknown" )?    (
                    <span className="count" title="Completed / Total">
                      {this.props.totalcount} 
                      {/* /{" "} */}
                      {/* {this.OnGroundData[this.state.selectedVan]} */}
                    </span>
                  ): (
                  <span className="count" title="Completed">
                    {this.props.totalcount}
                  </span>
                )}
              </div>
              <div className="total-values">
                <a onClick={this.handleExport} className="export">
                  <img src="assets/images/export.png" /> Export
                </a>
              </div>
            </div>
          </div>
          <div className="analytics-main">
            <div
              className={
                this.props.registers &&
                Object.keys(this.props.registers).length > 35
                  ? "change-high"
                  : "change-low"
              }
              className="engagementsBar"
              style={{ marginTop: this.props.height }}
            >
              <div className="engagementsBar-in">
                <div className="eng-txt">No of Engagements</div>

                {this.props.registers && (
                  <UpdateActivation
                    fromDate={this.props.fromDate}
                    toDate={this.props.toDate}
                    locationData={this.props.locationtree}
                    location={this.props.location}
                    registers={this.props.registers}
                    campaigns={this.props.campaigns}
                    gender={this.state.gender}
                    gamestatus={this.state.gamestatus}
                    selectedVan={this.state.selectedVan}
                    datetimerange={this.state.datetimerange}
                  />
                )}
              </div>
            </div>

            {this.state.smssent && (
              <div className="show">SMS sent to the mobile number</div>
            )}
            <div className="eng-details">
              {this.props.campaigns && (
                <table className="details-table">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Number</th>
                      <th>Activation</th>
                      <th>Location</th>
                      <th>Date</th>
                      <th>Time</th>
                      <th>Capture</th>
                    </tr>
                  </thead>

                  <tbody>
                    <UpdateActivationList
                      campaigns={this.props.campaigns}
                      //  newCampaigns={this.props.newCampaigns}
                      actvalue={this.props.actvalue}
                      rangevalue={this.props.rangevalue}
                      fromdate={this.props.fromdate}
                      todate={this.props.todate}
                      nextCampaigns={this.props.nextCampaigns}
                      detail={this.detail}
                      gender={this.state.gender}
                      picture={this.picture}
                      handleSms={this.handleSms}
                      totalCampaigns={this.state.totalCampaigns}
                    />
                  </tbody>
                </table>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    actvalue: state.activeFilter.actvalue,
    rangevalue: state.activeFilter.rangevalue,
    campaigns: state.activeFilter.campaigns,
    newCampaigns: state.activeFilter.newcampaigns,
    nextCampaigns: state.activeFilter.nextcampaigns,
    fromdate: state.activeFilter.fromDate,
    todate: state.activeFilter.toDate,
    registers: state.activeFilter.registers,
    totalcount: state.activeFilter.totalcount,
    height: state.activeFilter.height,
    initregisters: state.activeFilter.initregisters,
    maxValue: state.activeFilter.maxValue,
    login: state.activeFilter.login,
    campaintree: state.activeFilter.campaintree,
    locationtree:state.activeFilter.locationtree,
    location:state.activeFilter.location


  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      setlogin: setlogin,
      setcampaigns: setcampaigns,
      setlocationtree:setlocationtree,
      setcampainstree: setcampainstree,
      setnewcampaigns: setnewcampaigns,
      setnextcampaigns: setnextcampaigns,
      setinitregisters: setinitregisters,
      setregisters: setregisters,
      settotalcount: settotalcount,
      setheight: setheight,
      activation: activation,
      range: range,
      setfromdate: setfromdate,
      settodate: settodate,
      setgender: setgender,
      setfromtime: setfromtime,
      settotime: settotime,
      setwindowmaxvalue: setwindowmaxvalue,
      setinitregistersLoc: setinitregistersLoc,
      setregistersLoc: setregistersLoc
    },
    dispatch
  );
}

export default withAuth(
  withRouter(
    connect(
      mapStateToProps,
      matchDispatchToProps
    )(Analytics)
  )
);
