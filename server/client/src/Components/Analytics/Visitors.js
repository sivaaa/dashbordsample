let visitorData =  { 
    'MP1':  {
        'JNTU' : 240,
        'PragathiNagar': 80
    },
    'MP2':  {
        'Kondapur' : 160 ,
        'Kphb' : 240
    },
    'MH3': {
        'Manikonda' : 440,
        'Towlichowki' : 240
    },
    'RAJ1': {
        'Lingampalli' : 360,
        'Balanagar' : 80
    },
    'RAJ2': {
        'Nizampet' : 80,
        'Kokapet' :240
    },
    'RAJ3': {
        'Mehadipatnam' : 60,
        'Langerhouse': 80
    },
    'RAJ4': {
        'Begumpet' : 160,
        'Ameerpet' : 240
    },
    'HAR1': {
        'Alwal' : 240,
        'Tirumulgiri' : 360
    },
    'PUN1': {
        'Paradise' : 320,
        'Ranigunj' : 160
    },
    'MH1': {
        'Uppal' : 240,
        'Bodupal' : 80
    },
    'MH2': {
        'Ramnagar' : 80,
        'Amberpet' : 160
    },
    'MH4': {
        'ECIL' : 320,
        'DefenceColony' : 360
    }
};

export default visitorData;