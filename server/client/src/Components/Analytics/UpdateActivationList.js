import React from 'react';
import moment from 'moment';
import InfiniteLoader from 'react-infinite-loader'

const UpdateActivationList = ({totalCampaigns, loader, campaigns, gender, actvalue, rangevalue, fromdate, todate, nextCampaigns, detail, picture, handleSms }) => {

    //  console.log('hii update activation list', campaigns,gender,actvalue,rangevalue,fromdate,todate,city,nextCampaigns,detail,picture,handleSms)
    console.log(actvalue, rangevalue)
   
    if (actvalue === 'all') {
        if (fromdate === '' && todate === '') {

            if (campaigns.length > 0) {
                console.log(campaigns);
                return campaigns.slice(0, totalCampaigns).map((campaign, index) => {
                    let Date = moment(campaign.sessionTime).format("YYYY-MM-DD");
                    let Time = moment(campaign.sessionTime).format("HH:mm");
                    let visitorData = campaign.visitorData;


                    return (
                        <tr key={index} onClick={(e) => detail(e, campaign)}>
                            <td data-th="Name">{visitorData.name}</td>
                            <td data-th="Email">{visitorData.email ? visitorData.email : "-"}</td>
                            <td data-th="Number">{visitorData.mobile}</td>
                            <td data-th="Activation">  {visitorData.activation !== undefined ? visitorData.activation : 'DND'}</td>
                            <td data-th="Location">{visitorData.location}</td>
                            <td data-th="Date">{Date}</td>
                            <td data-th="Time">{Time}</td>
                            <td className="download" data-th="Capture"><a onClick={(e) => { e.stopPropagation(); picture(e, visitorData.picture) }
                            } ><img src="assets/images/download.png" /></a></td>
                            {index + 1 == totalCampaigns && <td className="loadimg">
                                <InfiniteLoader onVisited={(e) => {handleSms(e,totalCampaigns) }} />
                            </td>}
                            {index + 1 !== totalCampaigns && <td className="loadimg">
                                 
                            </td>}
                        </tr>

                    )
                })
            } else {
                return null;
            }
        } else {
            if (nextCampaigns !== undefined && nextCampaigns.length > 0) {
                return nextCampaigns.map((campaign, index) => {
                    let visitorData = campaign.visitorData;
                    let Date = moment(campaign.sessionTime).format("YYYY-MM-DD");
                    let Time = moment(campaign.sessionTime).format("HH:mm");
                    return (
                        <tr key={index} onClick={(e) => detail(e, campaign)}>
                            <td data-th="Name">{visitorData.name}</td>
                            <td data-th="Email">{visitorData.email ? visitorData.email : "-"}</td>
                            <td data-th="Number">{visitorData.mobile}</td>
                            <td data-th="Activation">  {visitorData.activation !== undefined ? visitorData.activation : 'DND'}</td>
                            <td data-th="Location">{visitorData.location}</td>
                            <td data-th="Date">{Date}</td>
                            <td data-th="Time">{Time}</td>
                            <td className="download" data-th="Capture"><a onClick={(e) => { e.stopPropagation(); picture(e, visitorData.picture) }
                            } ><img src="assets/images/download.png" /></a></td>
                           {index + 1 == totalCampaigns && <td className="loadimg">
                                <InfiniteLoader onVisited={(e) => {handleSms(e,totalCampaigns) }} />
                            </td>}
                            {index + 1 !== totalCampaigns && <td className="loadimg">
                                 
                            </td>}
                        </tr>
                    )
                })
            } else {
                return null;
            }
        }

    } else {
        if (nextCampaigns.length > 0) {
            return nextCampaigns.map((campaign, index) => {
                // let visitorData = campaign.visitorData;
                // var m = campaign.visitorData.date_time.split('T');
                let visitorData = campaign.visitorData;
                let Date = moment(campaign.sessionTime).format("YYYY-MM-DD");
                let Time = moment(campaign.sessionTime).format("HH:mm");
                console.log(campaign.sessionTime, Date)


                if (campaign.visitorData.activation == actvalue) {
                    return (
                        <tr key={index} onClick={(e) => detail(e, campaign)}>
                            <td data-th="Name">{visitorData.name}</td>
                            <td data-th="Email">{visitorData.email ? visitorData.email : "-"}</td>
                            <td data-th="Number">{visitorData.mobile}</td>
                            <td data-th="Activation">  {visitorData.activation !== undefined ? visitorData.activation : 'DND'}</td>
                            <td data-th="Location">{visitorData.location}</td>
                            {/* <td data-th="Gender">{visitorData.gender}</td> */}
                            {/* <td data-th="Area">{visitorData.city}</td> */}
                            {/* <td data-th="'Yes' answered">{visitorData.AnsweredY !== undefined ? visitorData.AnsweredY : '0'}</td> */}
                            <td data-th="Date">{Date}</td>
                            <td data-th="Time">{Time}</td>
                            <td className="download" data-th="Capture"><a onClick={(e) => { e.stopPropagation(); picture(e, visitorData.picture) }
                            } ><img src="assets/images/download.png" /></a></td>
                            {/* <td className="sms" data-th=""><a onClick={(e) => 
                                {e.stopPropagation();handleSms(e,visitorData.mobile,visitorData.shortcode)}
                                }><img src="assets/images/sms.png" /></a></td> */}
                        </tr>
                    )
                }
            })
        } else {
            return null;
        }
    }

}


export default UpdateActivationList;
