import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Header from "../Header/Header";
import AuthService from './../../routers/AuthService';
const request = require('superagent');

class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            smssent:false,
            analytics:false,
            visitor:'',
        }
        this.logout = this.logout.bind(this);
        this.apiLink=process.env.REACT_APP_SECRET_CODE.trim();

    }
    logout(e){
        e.preventDefault();
        const Auth = new AuthService('http://api.wowso.me/tvs');
        Auth.logout();
        this.props.history.push({
            pathname: '/advlogin'
        })
    }   
    picture(e,url){  
        var milliseconds = (new Date).getTime();
        var xhr = new XMLHttpRequest();
        var img = url.replace(/^https:\/\//i, 'http://');
        xhr.open("GET", img, true);
        xhr.responseType = "blob";
        xhr.onload = function(){
            var urlCreator = window.URL || window.webkitURL;
            var imageUrl = urlCreator.createObjectURL(this.response);
            var tag = document.createElement('a');
            tag.href = imageUrl;
            tag.download = milliseconds+'.png';
            document.body.appendChild(tag);
            tag.click();
            document.body.removeChild(tag);
        }
        xhr.send();
    }
    handleSms(e,mobile,shortcode){
        request
        .post(this.apiLink+'tvs/sendsms')
        .send({mobile:mobile,shortcode:shortcode})
        .then(res => {
            console.log('sent sms success');
            this.setState({
                smssent:true
            })
        })
    }
    componentDidMount() {
        console.log(this.props.match);
        if(this.props.match.path === '/gallery/:id'){
            this.setState({
                visitor:'manager'
            })
        }else{
            this.setState({
                visitor:'user'
            })
        }
        request
            .post(this.apiLink+'oc/activation/GetVisitor')
            .send({shortcode:this.props.match.params.id})
            .then(res => {
                let campaign = JSON.parse(res.text);
                this.setState({
                    campaign: campaign[0]
                })
            })
            .catch(e => {
                console.log(e)
            })
           
    }
    componentDidUpdate(){
        setTimeout(() => {
            this.setState({
                smssent:false
            })
        }, 5000);
    }
    render() {
        let {campaign = {}} = this.state;
        let visitorData,Date,Time;
        const Auth = new AuthService('http://api.wowso.me/tvs');
        //var m = campaign.visitorData !== undefined ? campaign.visitorData.date_time.split('T') : '';
        if(campaign.visitorData !== undefined){
            let visitorData = campaign.visitorData;                            
            let Date = moment(campaign.visitorData.date_time).format("YYYY-MM-DD");
            let Time = moment(campaign.visitorData.date_time).format("HH:SS");
        }
        
        return (            
            <div>
                {this.state.visitor === 'manager' && Auth.loggedIn() && 
                 <Header pageType="exitometer" analytics={this.state.analytics} /> }
                       

                {/* <div className="header">
                    <div className="header-main">
                        <div className="logo">
                            <img src="/assets/images/logo.jpg" />
                        </div>
                        {this.state.visitor === 'manager' && Auth.loggedIn() && <div className="header-links">                            
                            <a onClick={this.logout}>Logout</a>
                        </div> }
                    </div>
                </div> */}
                {campaign.visitorData !== undefined  && <div className="main">
                    <div className="act-details-main">
                        <div className="act-details-top">
                            <span className="act-no">{campaign.visitorData.activation}</span>
                            <span className="act-date">{Date}</span>
                            <span className="act-time">{Time}</span>
                            <span className="act-place">{campaign.visitorData.location}</span>
                        </div>
                        {campaign && campaign.visitorData && campaign.visitorData.picture &&
                          <div className="act-img">
                          <img src={campaign.visitorData.picture} />
                      </div>}
                      
                        <div className="act-details-btm">
                            <div className="act-details-btm-left">
                                <span className="name">{campaign.visitorData.name}</span>
                                <span className="phone">{campaign.visitorData.mobile}</span>
                            </div>
                            <div className="act-details-btm-right">
                                <a className="download" onClick={(e)=>this.picture(e,campaign.visitorData.picture)}><img src="/assets/images/downloadImg.png" /></a>
                            </div>
                        </div>
                        {this.state.smssent && <div className='show'>SMS sent to the mobile number</div>}
                    </div>

                  
                </div>}
            </div>
        );
    }
}

Contact.propTypes = {
};

export default Contact;
