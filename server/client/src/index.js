import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import AppRouter from './routers/AppRouter';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import {createLogger} from 'redux-logger';
import allReducers from './reducers';
import {loadState,saveState } from './localStorage';
import './index.css';

const logger = createLogger();
const persistedState = loadState();

const store = createStore(
    allReducers,
    persistedState,
    applyMiddleware(thunk, promise, logger)
);

store.subscribe(() => {
    //this is just a function that saves state to localStorage
    saveState(store.getState());
});

ReactDOM.render(<Provider store={store}>
                    <AppRouter />
                </Provider>, 
    document.getElementById('root'));
registerServiceWorker();
