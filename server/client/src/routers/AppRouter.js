import React from 'react';
import { BrowserRouter, Route, Switch,Redirect } from 'react-router-dom';
import Contact from './../Components/Contact/Contact';
import Gallery from './../Components/Gallery/Gallery';
import AdvanceData from '../Components/Analytics/AdvanceData';
import AdvLogin from './../Components/Login/AdvLogin';
import Yourmovie from './../Components/Yourmovie/dashboard';
import MovieContact from './../Components/Yourmovie/moviecontact';
import Location from './../Components/Locations/location'
import './AppRouter.css';
import NotFoundPage from '../NotFoundPage';

const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Switch>
      <Route path="/addlocations" component={Location} exact={true} />
        <Route path="/advlogin" component={AdvLogin} exact={true} />
        <Route key="gallery" path="/movie/gallery" component={Yourmovie} exact={true} />   
        <Route key="dashboard" path="/yourmovie" component={Yourmovie} exact={true} />   
        <Route key="record" path="/record/:id" component={Yourmovie} exact={true} />   
        <Route key="movie" path="/yourmovie/:id" component={Yourmovie} exact={true} />   
        <Route path="/" component={AdvanceData} exact={true} />
        <Route path="/home" component={AdvanceData} exact={true} />
        <Route path="/gallery" component={Gallery} exact={true} />
        <Route path="/advance" component={AdvanceData} exact={true} />
        <Route path="/movieadvance" component={Yourmovie} exact={true} />
        <Route path="/:id" component={Contact} exact={true} />
        <Route path="/gallery/:id" component={Contact} exact={true} />
        <Route path="movie/gallery/:id" component={MovieContact} exact={true} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </BrowserRouter>
);

export default AppRouter;
