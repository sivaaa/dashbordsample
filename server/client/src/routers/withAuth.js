import React, { Component } from 'react';
import AuthService from './AuthService';

export default function withAuth(AuthComponent) {
    const Auth = new AuthService('http://api.wowso.me/tvs');
    return class AuthWrapped extends Component {
                constructor() {
                    super();
                    this.state = {
                        user: null
                    }
                }
                componentDidMount() {
                    if (!Auth.loggedIn()) {
                        if(this.props.history !== undefined){
                            this.props.history.replace('/advlogin')
                        }
                    }
                    else {
                        try {
                            const profile = Auth.getProfile()
                            this.setState({
                                user: profile
                            })
                        }
                        catch(err){
                            Auth.logout()
                            this.props.history.replace('/advlogin')
                        }
                    }
                }
                render() {
                    if (this.state.user) {
                        return (
                            <AuthComponent history={this.props.history} user={this.state.user} />
                        )
                    }
                    else {
                        return null
                    }
                }
            }
}