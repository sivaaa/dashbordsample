export const setcampaigns = (campaigns) => {
    return {
        type: 'CAMPAIGNS',
        payload: campaigns
    }
};

export const setnewcampaigns = (newcampaigns) => {
    return {
        type: 'NEWCAMPAIGNS',
        payload: newcampaigns
    }
};
export const setlandingpage =(landingpage)=>{
    return {
        type: 'LANDING_PAGE',
        payload: landingpage
    }
};

export const setnextcampaigns = (nextcampaigns) => {
    return {
        type: 'NEXTCAMPAIGNS',
        payload: nextcampaigns
    }
};

export const activation = (actvalue) => {
    return {
        type: 'ACTIVATION_SELECTED',
        payload: actvalue
    }
};

export const range = (rangevalue) => {
    return {
        type: 'RANGE_SELECTED',
        payload: rangevalue
    }
};

export const setfromdate = (fromdate) => {
    return {
        type: 'FROM_DATE',
        payload: fromdate
    }
};

export const settodate = (todate) => {
    return {
        type: 'TO_DATE',
        payload: todate
    }
};
export const setgender = (gender) => {
    return {
        type: 'Gender',
        payload: gender
    }
};

export const setfromtime = (fromtime) => {
    return {
        type: 'FROM_TIME',
        payload: fromtime
    }
};

export const settotime = (totime) => {
    return {
        type: 'TO_TIME',
        payload: totime
    }
};

export const setregisters = (registers) => {
    return {
        type: 'REGISTERS',
        payload: registers
    }
};

export const setinitregisters = (registers) => {
    return {
        type: 'INIT_REGISTERS',
        payload: registers
    }
};

export const setregistersLoc = (registers) => {
    return {
        type: 'REGISTERSLOC',
        payload: registers
    }
};

export const setinitregistersLoc = (registers) => {
    return {
        type: 'INIT_REGISTERS_LOC',
        payload: registers
    }
};

export const settotalcount = (totalcount) => {
    return {
        type: 'TOTAL_COUNT',
        payload: totalcount
    }
};

export const setmobile = (mobile) => {
    return {
        type: 'MOBILE_NUMBER',
        payload: mobile
    }
};

export const setmobilecampaigns = (mcampaigns) => {
    return {
        type: 'MOBILE_CAMPAIGNS',
        payload: mcampaigns
    }
};

export const setheight = (height) => {
    return {
        type: 'WINDOW_HEIGHT',
        payload: height
    }
};

export const setwindowmaxvalue = (maxvalue) => {
    return {
        type: 'WINDOW_MAXVALUE',
        payload: maxvalue
    }
};

export const setanalytics = (analytics) => {
    return {
        type: 'ANALYTICS',
        payload: analytics
    }
};

export const setgallery = (gallery) => {
    return {
        type: 'GALLERY',
        payload: gallery
    }
};

export const setlogout = (logout) => {
    return {
        type: 'LOGOUT',
        payload: logout
    }
};

export const setlogin = (login) => {
    return {
        type: 'LOGIN',
        payload: login
    }
};
export const setcampainstree=(campaintree)=>{
  //  console.log(campaintree);
    return{
        type:'CAMPAIN_TREE',
        payload:campaintree
    }
}
export const setlocationtree=(locationtree)=>{
    //  console.log(campaintree);
      return{
          type:'LOCATION_TREE',
          payload:locationtree
      }
  }
  export const setLocation=(location)=>{
    return{
        type:'LOCATION',
        payload:location
    }
  }
