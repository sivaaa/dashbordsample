import {combineReducers} from 'redux';
import FilterReducer from './reducer-filters';
import ActiveFilterReducer from './reducer-active-filter';

/*
 * We combine all reducers into a single object before updated data is dispatched (sent) to store
 * Your entire applications state (store) is just whatever gets returned from all your reducers
 * */

const allReducers = combineReducers({
    // filters: FilterReducer,
    activeFilter: ActiveFilterReducer
});

export default allReducers