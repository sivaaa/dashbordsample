/*
 * All reducers get two parameters passed in, state and action that occurred
 *       > state isn't entire apps state, only the part of state that this reducer is responsible for
 * */

// "state = null" is set so that we don't throw an error when app first boots up

const expensesReducerDefaultState = {
    campaintree:[],
    actvalue: 'all',
    rangevalue:'all',
    fromDate: "",
    toDate: "",
    fromTime: "",
    toTime: "",
    campaigns:"",
    newcampaigns:"",
    initregisters:"",
    registers:"",
    totalcount:0,
    nextcampaigns:"",
    mobile:"",
    mobileCampaigns:"",
    maxValue:"",
    analytics:true,
    gallery:false,
    logout:false,
    login:false
};

export default function (state = {}, action) {
    switch (action.type) {
        case 'CAMPAIN_TREE':
            return {
                ...state,
                campaintree:action.payload,
               
            };
            break;
            case 'LANDING_PAGE':
                return {
                    ...state,
                    landingpage:action.payload,
                   
                };
                break;
            case 'LOCATION':
                    return {
                        ...state,
                        location:action.payload,
                       
                    };
                    break;
            case 'LOCATION_TREE':
                return {
                    ...state,
                    locationtree:action.payload,
                   
                };
                break;
        case 'ACTIVATION_SELECTED':
            return {
                ...state,
                actvalue:action.payload,
                // rangevalue : 'all',
                fromDate:'',
                toDate:'',
                fromTime:'',
                toTime:'',
                city:'all'
            };
            break;
        case 'RANGE_SELECTED':
            return {
                ...state,
                rangevalue:action.payload,
                fromDate:'',
                toDate:'',
                fromTime:'',
                toTime:'',
                city:'all'
                // actvalue:'all'
            };
            break;
        case 'FROM_DATE':
            return {
                ...state,
                fromDate:action.payload,
                // actvalue:'all',
                rangevalue:'all'
            };
            break;
        case 'TO_DATE':
            return {
                ...state,
                toDate:action.payload
            };
            break;
        case 'FROM_TIME':
            return {
                ...state,
                fromTime:action.payload
            };
            break;
        case 'TO_TIME':
            return {
                ...state,
                toTime:action.payload
            };
            break;
        case 'CAMPAIGNS':
            return {
                ...state,
                campaigns:action.payload
            };
            break;
        case 'NEWCAMPAIGNS':
            return {
                ...state,
                newcampaigns:action.payload,
            };
            break;
        case 'NEXTCAMPAIGNS':
            return {
                ...state,
                nextcampaigns:action.payload,
            };
            break;
        case 'REGISTERS':
            return {
                ...state,
                registers:action.payload,
            };
            break;
        case 'INIT_REGISTERS':
            return {
                ...state,
                initregisters:action.payload,
            };
            break;
        case 'REGISTERSLOC':
            return {
                ...state,
                registersLoc:action.payload,
            };
            break;
        case 'INIT_REGISTERS_LOC':
            return {
                ...state,
                initregistersLoc:action.payload,
            };
            break;
        case 'TOTAL_COUNT':
            return {
                ...state,
                totalcount: action.payload                
            };
            break;
        case 'MOBILE_NUMBER':
            return {
                ...state,
                mobile: action.payload,
                fromDate:'',
                toDate:'',
                fromTime:'',
                toTime:'',
                rangevalue:'all',
                actvalue:'all'                
            };
            break;
        case 'MOBILE_CAMPAIGNS':
            return {
                ...state,
                mobileCampaigns: action.payload                
            };
            break;
        case 'WINDOW_HEIGHT':
            return {
                ...state,
                height:action.payload
            }
        case 'WINDOW_MAXVALUE':
            return {
                ...state,
                maxValue:action.payload
            }
        case 'LOGOUT':
            return {
                actvalue: 'all',
                rangevalue:'all',
                fromDate: "",
                toDate: "",
                fromTime: "",
                toTime: "",
                campaigns:"",
                newcampaigns:"",
                initregisters:"",
                registers:"",
                totalcount:0,
                nextcampaigns:"",
                mobile:"",
                mobileCampaigns:"",
                maxValue:"",
                analytics:true,
                gallery:false,
                logout:action.payload
            }
        case 'ANALYTICS':
            return {
                ...state,
                analytics:action.payload
            }
        case 'GALLERY':
            return {
                ...state,
                gallery:action.payload
            }
        case 'LOGIN':
            return {
                ...state,
                login:action.payload
            }
    }
    
    return state;
}
