const express = require("express");
const router = express.Router();
const randomstring = require("randomstring");
const request = require("superagent");
const nodemailer = require("nodemailer");
const { activation } = require("../models/activation");
const { ocUser } = require("../models/ocUser");
const {movieUser} =require("../models/movieUser");
const instance = require('../config/instance');



/**************************************************************/
router.post("/register", (req, res) => {
  let campaign = req.body;
  let VanCity = campaign.visitorData.activation;
  if(campaign.visitorData.mobile){
  ocUser
    .findOne({ "visitorData.mobile": campaign.visitorData.mobile })
    .then(user => {
      if (user) {
        const campaign = req.body;
        const update = {
          currentState: campaign.currentState,
          sessionTime: campaign.sessionTime,
          mediaList: campaign.mediaList,
          visitorData: {
            mobile: campaign.visitorData.mobile,
            name: campaign.visitorData.name,
            email: campaign.visitorData.email,
            score:campaign.visitorData.score,
            picture:
              "https://s3-ap-southeast-1.amazonaws.com/originalchoice-dev/" +
              campaign.mediaList[0],
            location: campaign.visitorData.location,
            volunteer_mobile: campaign.visitorData.volunteer_mobile,
            activation: VanCity,
            shortcode:  user.visitorData.shortcode,
          
          },
          metadataPath: campaign.metadataPath,
          error: campaign.error
        };

        ocUser
          .findOneAndUpdate(
            { "visitorData.mobile": campaign.visitorData.mobile },
            update
          )
          .then(
            data => {

              let resData = data;
             
              // "Hello [Name],
              // Thank you for participating.Your photo with Original Choice here -[Link here]"
              request
                .get(
                  `http://103.16.101.52:8080/bulksms/bulksms?username=purr-wowsome&password=wow123&type=0&dlr=1&destination=${
                    campaign.visitorData.mobile
                  }&source=OCJDPL&message=Hello ${campaign.visitorData.name}, Thank you for participating. Your photo with Original Choice here ${
                    instance.webUrl+resData.visitorData.shortcode
                  }`
                )
                .then(res => {
                  console.log(
                    "Message sent successfully to user!",resData.visitorData.mobile
                  );
                })
                .catch(err => {
                  console.log("err sending sms", err);
                });
             
              if (campaign.visitorData.volunteer_mobile) {
              
                request
                  .get(
                    `http://103.16.101.52:8080/bulksms/bulksms?username=purr-wowsome&password=wow123&type=0&dlr=1&destination=${
                      resData.visitorData.volunteer_mobile
                    }&source=OCJDPL&message=Player Name: ${campaign.visitorData.name}, Phone Number: ${resData.visitorData.mobile},  Score:${campaign.visitorData.score}`
                  )
                  .then(res => {
                    console.log(
                      "Message sent successfully to Volunteer!",resData.visitorData.volunteer_mobile
                    );
                  })
                  .catch(err => {
                    console.log("err sending sms", err);
                  });
                return res.send("updated success!");
              }
              else{
                return res.send("updated success!");
              }
            },
            e => {
              console.log("err", e);
              res.status(400).send(e);
            }
          )
          .catch(e => {
            console.log("exp:", e);
          });
      } else {
        var shortcode = randomstring.generate(4);
        var newocUser = new ocUser({
          currentState: campaign.currentState,
          sessionTime: campaign.sessionTime,
          mediaList: campaign.mediaList,
          visitorData: {
            mobile: campaign.visitorData.mobile,
            name: campaign.visitorData.name,
            city: campaign.visitorData.city,
            email: campaign.visitorData.email,
            score:campaign.visitorData.score,
            picture:
              "https://s3-ap-southeast-1.amazonaws.com/originalchoice-dev/" +
              campaign.mediaList[0],
            location: campaign.visitorData.location,
            volunteer_mobile: campaign.visitorData.volunteer_mobile,
           // activation: campaign.visitorData.activation,
            activation: VanCity,
            shortcode: shortcode,
            
          },
          sessionName: campaign.sessionName,
          metadataPath: campaign.metadataPath,
          error: campaign.error
        });
        //console.log('tvs campaign:',tvs);
        newocUser
          .save()
          .then(
            data => {
              console.log("save success!", data);
              let resData = data;
              request
                .get(
                  `http://103.16.101.52:8080/bulksms/bulksms?username=purr-wowsome&password=wow123&type=0&dlr=1&destination=${
                    resData.visitorData.mobile
                  }&source=OCJDPL&message=Hello ${campaign.visitorData.name}, Thank you for participating. Your photo with Original Choice here ${
                    instance.webUrl+resData.visitorData.shortcode
                  }`
                )
                .then(res => {
                  console.log(
                    "Message sent successfully!",
                    instance.webUrl+resData.visitorData.shortcode
                  );
                });
                if(resData.visitorData.volunteer_mobile){
              request
                .get(
                  `http://103.16.101.52:8080/bulksms/bulksms?username=purr-wowsome&password=wow123&type=0&dlr=1&destination=${
                    resData.visitorData.volunteer_mobile
                  }&source=OCJDPL&message=Player Name ${campaign.visitorData.name}, Phone Number ${resData.visitorData.mobile},  Score:${resData.visitorData.score}`
                )
                .then(res => {
                  console.log(
                    "Message sent successfully!",
                    resData.visitorData.volunteer_mobile
                  );
                });
              }
              return res.send("updated success!");
            },
            e => {
              console.log("err", e);
              res.status(400).send(e);
            }
          )
          .catch(e => {
            console.log("exp:", e);
          });
      }
    });
  }
  else{
    console.log("guest");
    var shortcode = randomstring.generate(4);
    var newocUser = new ocUser({
      currentState: campaign.currentState,
      sessionTime: campaign.sessionTime,
      mediaList: campaign.mediaList,
      visitorData: {
        mobile:"-",
        name: campaign.visitorData.name,
        city: campaign.visitorData.city,
        email: campaign.visitorData.email,
        score:campaign.visitorData.score,
        picture:
          "https://s3-ap-southeast-1.amazonaws.com/originalchoice-dev/" +
          campaign.mediaList[0],
        location: campaign.visitorData.location,
        volunteer_mobile: campaign.visitorData.volunteer_mobile,
       // activation: campaign.visitorData.activation,
        activation: VanCity,
        shortcode: shortcode,
        
      },
      sessionName: campaign.sessionName,
      metadataPath: campaign.metadataPath,
      error: campaign.error
    });
    //console.log('tvs campaign:',tvs);
    newocUser
      .save()
      .then(
        data => {
          console.log("save success!", data);
          let resData = data;
          // console.log(resData)
            console.log(req.body);
            console.log(req.body);

            if(req.body.visitorData.volunteer_mobile){
          request
            .get(
              `http://103.16.101.52:8080/bulksms/bulksms?username=purr-wowsome&password=wow123&type=0&dlr=1&destination=${
                req.body.visitorData.volunteer_mobile
              }&source=OCJDPL&message=Player Name: ${resData.visitorData.name}, Phone Number: null,  Score:${resData.visitorData.score}`
            )
            .then(res => {
              console.log(
                "Message sent successfully!",
                req.body.visitorData.volunteer_mobile
              );
            });
          }
          return res.send("updated success!");
        },
        e => {
          console.log("err", e);
          res.status(400).send(e);
        }
      )
      .catch(e => {
        console.log("exp:", e);
      });
  }
});


router.post("/movieRegister", (req, res) => {
  let campaign = req.body;
  let VanCity = campaign.visitorData.activation;
  if(campaign.visitorData.mobile){
    movieUser
    .findOne({ "visitorData.mobile": campaign.visitorData.mobile })
    .then(user => {
      if (user) {
        const campaign = req.body;
        const update = {
          currentState: campaign.currentState,
          sessionTime: campaign.sessionTime,
          mediaList: campaign.mediaList,
          visitorData: {
            mobile: campaign.visitorData.mobile,
            name: campaign.visitorData.name,
            email: campaign.visitorData.email,
            picture:
              "https://s3-ap-southeast-1.amazonaws.com/yourmovie-oc/" +
              campaign.mediaList[0],
            location: campaign.visitorData.location,
            volunteer_mobile: campaign.visitorData.volunteer_mobile,
            activation: VanCity,
            shortcode:  user.visitorData.shortcode,
          
          },
          metadataPath: campaign.metadataPath,
          error: campaign.error
        };

        movieUser
          .findOneAndUpdate(
            { "visitorData.mobile": campaign.visitorData.mobile },
            update
          )
          .then(
            data => {

              let resData = data;
             
              // "Hello [Name],
              // Thank you for participating.Your photo with Original Choice here -[Link here]"
              request
                .get(
                  `http://103.16.101.52:8080/bulksms/bulksms?username=purr-wowsome&password=wow123&type=0&dlr=1&destination=${
                    campaign.visitorData.mobile
                  }&source=OCJDPL&message=Hello ${campaign.visitorData.name}, Thank you for participating. Your photo with Original Choice here ${
                    instance.webUrl+'yourmovie/'+resData.visitorData.shortcode
                  }`
                )
                .then(res => {
                  console.log(
                    "Message sent successfully to user!",resData.visitorData.mobile
                  );
                })
                .catch(err => {
                  console.log("err sending sms", err);
                });
             
              if (campaign.visitorData.volunteer_mobile) {
              
                request
                  .get(
                    `http://103.16.101.52:8080/bulksms/bulksms?username=purr-wowsome&password=wow123&type=0&dlr=1&destination=${
                      campaign.visitorData.volunteer_mobile
                    }&source=OCJDPL&message=Original Choice ${campaign.visitorData.location}, Player Name: ${campaign.visitorData.name}, Phone Number: ${resData.visitorData.mobile}`
                  )
                  .then(res => {
                    console.log(
                      "Message sent successfully to Volunteer!",resData.visitorData.volunteer_mobile
                    );
                  })
                  .catch(err => {
                    console.log("err sending sms", err);
                  });
                return res.send("updated success!");
              }
              else{
                return res.send("updated success!");
              }
            },
            e => {
              console.log("err", e);
              res.status(400).send(e);
            }
          )
          .catch(e => {
            console.log("exp:", e);
          });
      } else {
        var shortcode = randomstring.generate(4);
        var newmovieUser = new movieUser({
          currentState: campaign.currentState,
          sessionTime: campaign.sessionTime,
          mediaList: campaign.mediaList,
          visitorData: {
            mobile: campaign.visitorData.mobile,
            name: campaign.visitorData.name,
            city: campaign.visitorData.city,
            email: campaign.visitorData.email,
            score:campaign.visitorData.score,
            picture:
              "https://s3-ap-southeast-1.amazonaws.com/yourmovie-oc/" +
              campaign.mediaList[0],
            location: campaign.visitorData.location,
            volunteer_mobile: campaign.visitorData.volunteer_mobile,
           // activation: campaign.visitorData.activation,
            activation: VanCity,
            shortcode: shortcode,
            
          },
          sessionName: campaign.sessionName,
          metadataPath: campaign.metadataPath,
          error: campaign.error
        });
        //console.log('tvs campaign:',tvs);
        newmovieUser
          .save()
          .then(
            data => {
              console.log("save success!", data);
              let resData = data;
              request
                .get(
                  `http://103.16.101.52:8080/bulksms/bulksms?username=purr-wowsome&password=wow123&type=0&dlr=1&destination=${
                    resData.visitorData.mobile
                  }&source=OCJDPL&message=Hello ${campaign.visitorData.name}, Thank you for participating. Your photo with Original Choice here ${
                    instance.webUrl+'yourmovie/'+resData.visitorData.shortcode
                  }`
                )
                .then(res => {
                  console.log(
                    "Message sent successfully!",
                    instance.webUrl+resData.visitorData.shortcode
                  );
                });
                if(campaign.visitorData.volunteer_mobile){
              request
                .get(
                  `http://103.16.101.52:8080/bulksms/bulksms?username=purr-wowsome&password=wow123&type=0&dlr=1&destination=${
                    campaign.visitorData.volunteer_mobile
                  }&source=OCJDPL&message=Original Choice ${campaign.visitorData.location}, Player Name: ${campaign.visitorData.name}, Phone Number: ${resData.visitorData.mobile}`
                )
                .then(res => {
                  console.log(
                    "Message sent successfully!",
                    campaign.visitorData.volunteer_mobile
                  );
                });
              }
              return res.send("updated success!");
            },
            e => {
              console.log("err", e);
              res.status(400).send(e);
            }
          )
          .catch(e => {
            console.log("exp:", e);
          });
      }
    });
  }
  else{
    console.log("guest");
    var shortcode = randomstring.generate(4);
    var newmovieUser = new movieUser({
      currentState: campaign.currentState,
      sessionTime: campaign.sessionTime,
      mediaList: campaign.mediaList,
      visitorData: {
        mobile:"-",
        name: campaign.visitorData.name,
        city: campaign.visitorData.city,
        email: campaign.visitorData.email,
        score:campaign.visitorData.score,
        picture:
          "https://s3-ap-southeast-1.amazonaws.com/yourmovie-oc/" +
          campaign.mediaList[0],
        location: campaign.visitorData.location,
        volunteer_mobile: campaign.visitorData.volunteer_mobile,
       // activation: campaign.visitorData.activation,
        activation: VanCity,
        shortcode: shortcode,
        
      },
      sessionName: campaign.sessionName,
      metadataPath: campaign.metadataPath,
      error: campaign.error
    });
    //console.log('tvs campaign:',tvs);
    newmovieUser
      .save()
      .then(
        data => {
          console.log("save success!", data);
          let resData = data;
          // console.log(resData)
            console.log(req.body);
            console.log(req.body);

            if(campaign.visitorData.volunteer_mobile){
          request
            .get(
              `http://103.16.101.52:8080/bulksms/bulksms?username=purr-wowsome&password=wow123&type=0&dlr=1&destination=${
                campaign.visitorData.volunteer_mobile
              }&source=OCJDPL&message=Original Choice ${campaign.visitorData.location}  Player Name: ${campaign.visitorData.name}, Phone Number: ${resData.visitorData.mobile}`
            )
            .then(res => {
              console.log(
                "Message sent successfully!",
                campaign.visitorData.volunteer_mobile
              );
            });
          }
          return res.send("updated success!");
        },
        e => {
          console.log("err", e);
          res.status(400).send(e);
        }
      )
      .catch(e => {
        console.log("exp:", e);
      });
  }
});
router.post("/addLocations", (req, res) => {
  console.log("add locations");

  const { activationName, date, locations } = req.body;

  if (!activationName || !date || !locations === undefined) {
    return res.json({ success: false, message: "please send all details" });
  }

  let locationCount = 0;

  activation
    .findOne({
      activationName: activationName,
      date: date
    })
    .then(activationexists => {
      if (activationexists) {
        console.log("activation exists");
        for (let location of locations) {
          console.log("van exists");

          activation
            .findOne({
              activationName: activationName,
              date: date,
              "locations.locName": location.locName
            })
            .then(locationexists => {
              if (locationexists) {
                console.log("location exists");
                locationCount++;
                if (locationCount == locations.length) {
                  res.json({
                    success: true,
                    message: "Activation locations already exists"
                  });
                }
              } else {
                console.log("location doesnt exists", location);

                activation
                  .updateOne(
                    {
                      activationName: activationName,
                      date: date
                    },
                    {
                      $push: {
                        locations: location
                      }
                    }
                  )
                  .then(updated => {
                    console.log("new location added");
                    locationCount++;
                    if (locationCount == locations.length) {
                      res.json({
                        success: true,
                        message: "Activation location added"
                      });
                    }
                  })
                  .catch(err => {
                    console.log(err);
                    res.json({
                      success: false,
                      message: err
                    });
                  });
              }
            })
            .catch(err => {
              console.log(err);
              res.json({
                success: false,
                message: err
              });
            });
        }
      } else {
        console.log("new Activation added");

        const newActivation = new activation(req.body);

        // console.log(newActivation)
        newActivation
          .save()
          .then(data => {
            res.json({
              success: true,
              message: "Activation location added"
            });
            console.log("Activation data saved");
          })
          .catch(err => {
            console.log(err);
          });
      }
    })
    .catch(err => {
      console.log(err);
      res.json({
        success: false,
        message: err
      });
    });
});

router.get("/", (req, res) => {
  activation.aggregate(
    [
      { $group: { _id: null, activations: { $addToSet: "$activationName" } } },
      { $project: { _id: 0, activations: 1 } }
    ],
    function(err, result) {
      console.log(err, result);
      if (result) {
        if (result[0]) {
          res.json({
            success: true,
            message: result[0]
          });
        }
      } else {
        res.json({
          success: true,
          message: result
        });
      }
    }
  );
});
router.get("/analytics",(req,res) =>{
  movieUser.aggregate([
    {$group:{_id:"$visitorData.activation",location:{$addToSet:"$visitorData.location"},total:{$sum: 1}}},
    { $project: { _id: 0, activation: "$_id",location:1,total:1 } }

  ],
  function(err, result) {
    console.log(err, result);
    if (result) {
      if (result) {
        res.json({
          success: true,
          message: result
        });
      }
    } else {
      res.json({
        success: true,
        message: "no result"
      });
    }
  }
  )
})
router.get("/locations", (req, res) => {
  activation.aggregate(
    [
      { $group: { _id: null, vanNames: { $addToSet: "$activationName" },count:{$sum: 1} } },
      { $project: { _id: 0, vanNames: 1,count:1 } }
    ],
    function(err, result) {
      console.log(err, result);
      if (result) {
        if (result[0]) {
          res.json({
            success: true,
            message: result[0]
          });
        }
      } else {
        res.json({
          success: true,
          message: result
        });
      }
    }
  );
});
router.post("/filteredData",async (req,res)=>{
 const {activation,location,fromdate,todate}=req.body
  console.log(req.body);
 if(activation && location){
  movieUser.find({"visitorData.activation":activation,"visitorData.location":location}).sort({updatedAt:1}).then(datalist=>{
    if(datalist){
      return res.send({dataList:datalist});

    }
  })
  .catch(err => {
    console.log(err);
    res.json({
      success: false,
      message: err
    });
  });
}
else if(activation){
  movieUser.find({"visitorData.activation":activation}).sort({updatedAt:1}).then(datalist=>{
    if(datalist){
      return res.send({dataList:datalist});

    }
  })
  .catch(err => {
    console.log(err);
    res.json({
      success: false,
      message: err
    });
  });
}
else{
  movieUser.find({}).sort({updatedAt:1}).then(datalist=>{
    if(datalist){
      return res.send({dataList:datalist});

    }
  })
  .catch(err => {
    console.log(err);
    res.json({
      success: false,
      message: err
    });
  });
}
})

router.get("/GetVisitors", (req, res) => {
  ocUser
    .find({
      // shortcode: req.user.shortcode
    })
    .then(
      todos => {
        res.send({ todos });
      },
      e => {
        res.status(400).send(e);
      }
    );
});
router.post("/getLocations", (req, res) => {
  const { activationName, date } = req.body;

  if (!activationName) {
    return res.json({ success: false, message: "please send activationName" });
  }
  console.log(req.body);
  if (date && activationName) {
    activation.aggregate(
      [
        {
          $match: {
            activationName: activationName,
            date: date
          }
        },
        {
          $project: {
            _id: 0,
            "locations.locName": 1
          }
        }
      ],
      function(err, result) {
        // console.log(err, result);
        if (err) {
          res.json({
            success: false,
            message: err
          });
        } else if (result) {
          if (result[0]) {
            res.json({
              success: true,
              message: result[0].locations
            });
          } else {
            res.json({
              success: true,
              message: result
            });
          }
        }
      }
    );
  } else if (activationName === "all") {
    activation
      .find()
      .then(locations => {
        res.json({
          success: true,
          message: locations
        });
      })
      .catch(err => {
        console.log(err);
        res.json({
          success: false,
          message: err
        });
      });
  } else if (activationName) {
    activation.aggregate(
      [
        { $match: { activationName: activationName } },
        {
          $group: { _id: null, locations: { $addToSet: "$locations.locName" } }
        },
       
      ],
      function(err, result) {
        console.log(result);
        if (err) {
          console.log(err);
          res.json({
            success: false,
            message: err
          });
        } else if (result) {
          if (result[0]) {
            let newArray = [].concat.apply([], result[0].locations);
            // newArray = _.uniq(newArray);
            console.log(newArray);
            res.json({
              success: true,
              message: newArray
            });
          } else {
            res.json({
              success: true,
              message: result
            });
          }
        }
      }
    );
  }
});
router.post("/getLocationsByDate",(req,res)=>{
  const {date}=req.body;
  if(date){
    activation.aggregate(
      [
        {$match:{date:date}},
        {
          $group: { _id: null, activations: { $addToSet: "$activationName" } },
          
        },
        { $project: { _id: 0, activations: 1 } }

       
      ],
      function(err, result) {
        console.log(result);
        if (err) {
          console.log(err);
          res.json({
            success: false,
            message: err
          });
        } else if (result) {
          if (result[0]) {
            res.json({
              success: true,
              message: result[0].activations,
            });
          } else {
            res.json({
              success: true,
              message: result
            });
          }
        }
      }
    );
  }
  else{
    res.status(400).send({"message":"please send date"});

  }
})
router.get('/getuniquecities',(req,res) => {
  ocUser.distinct(
	"visitorData.activation"
  ).then((todos) => {
    res.send({ todos });
  }, (e) => {
    res.status(400).send(e);
  });
});

router.post('/csvexport',(req,res) => {
  let {_campaigns} = req.body;

  ocUser.find({
          "_id": {"$in" : _campaigns}
  },{"visitorData":1}).then((todos) => {
    console.log(todos);
let objKey;
    let csvData=[];
todos.map((campaign) => { 
//console.log(Object.keys((campaign.visitorData).toJSON()));
let campaignData = []
if(objKey === undefined){
  objKey = Object.keys((campaign.visitorData).toJSON());
  csvData.push(objKey.join(","))
}
for(let key of objKey){
  let value = campaign.visitorData[key];
  campaignData.push(value !== null ? value.replace(/,/g," ") : '');
}
csvData.push(campaignData.join(","))

})
          let textData = csvData.join('\n');
          res.set('Content-Type', 'application/csv');
res.status(200).send(textData);
  })
  .catch((e) => {
          res.status(400).send({message:e});
  });
});

router.post('/GetVisitor', (req, res) => {
  ocUser.find({
	"visitorData.shortcode":  req.body.shortcode	
  }).then((data) => {
    res.send(data);
  }, (e) => {
    res.status(400).send(e);
  });
});
router.post('/GetMovieVisitor', (req, res) => {
  movieUser.find({
	"visitorData.shortcode":  req.body.shortcode	
  }).then((data) => {
    res.send(data);
  }, (e) => {
    res.status(400).send(e);
  });
});

router.post("/tvs/updateCrowdCount", (req, res) => {
  const { activationName, date, locName, crowdCount } = req.body;

  if (!activationName || !date || !locName || crowdCount === undefined) {
    return res.json({ success: false, message: "please send all details" });
  }
  // console.log(activationName, date, locName, crowdCount)
  vanLocation
    .findOneAndUpdate(
      {
        activationName: activationName,
        date: date,
        "locations.locName": locName
      },
      {
        $set: {
          "locations.$.crowdCount": crowdCount
        }
      }
    )
    .then(location => {
      if (location) {
        // console.log("location exists");
        res.json({
          success: true,
          message: "Crowd Count updated"
        });
      } else {
        console.log("location doesnt exists");
        res.json({
          success: true,
          message: "Van doesnt exists for that location on that date"
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.json({
        success: false,
        message: err
      });
    });
});

router.post("/tvs/updateLocations", (req, res) => {
  const { activationName, date, locations } = req.body;

  if (!activationName || !date || !locations) {
    return res.json({ success: false, message: "please send all details" });
  }
  // console.log(activationName, date, locName, crowdCount)
  vanLocation
    .updateOne(
      {
        activationName: activationName,
        date: date
      },
      {
        $set: {
          locations: locations
        }
      }
    )
    .then(updated => {
      if (updated.nModified) {
        console.log("Locations updated", updated);
        res.json({
          success: true,
          message: "Locations updated"
        });
      } else {
        console.log("location doesnt exists");
        res.json({
          success: false,
          message: "Unable to update locations"
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.json({
        success: false,
        message: err
      });
    });
});

module.exports = router;
