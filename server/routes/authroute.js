

const {User} = require("../models/user");
const express = require("express");

const router = express.Router();

/*********************  Middleware *****************************************/
function middlewareForReg(req, res, next) {
  const { email, password } = req.body;
  if (!email || !password) {
    res.json({
      status: false,
      message: "Please send all details needed for registration"
    });
  } else {
    next();
  }
}


// POST /users
router.post("/", (req, res) => {
console.log('hii')
  const {email, password} = req.body;

  var user = new User({email: email, password: password});
  user
    .save()
    .then(() => {
      console.log('hello')
      return user.generateAuthToken();
    })
    .then(token => {
      res.header("x-auth", token).send(user);
    })
    .catch(e => {
      res.status(400).send(e);
    });
});


router.post("/login", (req, res) => {
  const {email, password} = req.body;
  console.log('login')
  User.findByCredentials(email, password)
    .then(user => {
      return user.generateAuthToken().then(token => {
        res.header("x-auth", token).send(user);
      });
    })
    .catch(e => {
      console.log('error', e)
      res.status(400).send(e);
    });
});



/**************************************************************/
router.get("/get", async (req, res) => {
  const projection = { email: 1 };
  const users = await User.find({}, projection);
  if (users) {
    res.send({ success: true, message: users });
  }
});

/**************************************************************/
router.get("/logout", function(req, res) {
  req.logout();
  res.json({ success: true, message: "Logged Out successfully" });
});

module.exports = router;
