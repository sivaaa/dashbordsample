
/***Module dependencies*/
const app = require('./app'),
      instance = require("./config/instance");

//==============================================================================
/***Module Variables*/
const port = process.env.PORT || instance.port,
       instanceName = process.env.instance || instance.instance;

//==============================================================================
/***Create server instance*/
const server = app.listen(instance.port, function() {
  console.log('server listening on port:' + port +' in ' + instanceName +
  ' mode');
});



